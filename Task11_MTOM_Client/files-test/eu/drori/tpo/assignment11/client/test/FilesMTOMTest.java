package eu.drori.tpo.assignment11.client.test;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.drori.tpo.assignment11.client.FileElement;
import eu.drori.tpo.assignment11.client.proxy.FileProxy;

public class FilesMTOMTest {
	private FileProxy _proxy;
	
	@Before
	public void before() {
		_proxy = new FileProxy();
	}
	
	@Test
	public void uploadAndDownload() {
		Path testFilePath = Paths.get("test.txt");
		try {
			if (Files.exists(testFilePath)) {
				long fileSize = Files.size(testFilePath);
				String fileName = testFilePath.getFileName().toString();
				byte[] fileContents = Files.readAllBytes(testFilePath);
				FileElement fileElement = new FileElement();
				fileElement.setFileName(fileName);
				fileElement.setFileSize(fileSize);
				fileElement.setFileContents(fileContents);
				String[] keywordsArr = {"keyword"};
				_proxy.uploadNewFile(fileElement, Arrays.asList(keywordsArr));
				byte[] fileContentsFromServer = _proxy.downloadFile("test.txt");
				Assert.assertArrayEquals(fileContents, fileContentsFromServer);
			} else {
				Assert.fail();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
	
	@Test
	public void getListOfFilesWithKeyword() {
		Path testFilePath = Paths.get("test.txt");
		FileElement fileElement1= new FileElement();
		try {
			if (Files.exists(testFilePath)) {
				fileElement1.setFileName(testFilePath.getFileName().toString());
				fileElement1.setFileSize(Files.size(testFilePath));
				List<FileElement> fileElementList = _proxy.getListOfFilesWithKeyword("keyword");
				FileElement fileElementFromServer = fileElementList.get(0);
				Assert.assertEquals(fileElement1.getFileName(), fileElementFromServer.getFileName());
				Assert.assertEquals(fileElement1.getFileSize(),fileElementFromServer.getFileSize());
			} else {
				Assert.fail();
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}
}
