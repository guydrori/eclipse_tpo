
package eu.drori.tpo.assignment11.client;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "FileServiceService", targetNamespace = "http://assignment11.tpo.drori.eu/", wsdlLocation = "http://localhost:8080/Task11_SOAP_MTOM/files?wsdl")
public class FileServiceService
    extends Service
{

    private final static URL FILESERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException FILESERVICESERVICE_EXCEPTION;
    private final static QName FILESERVICESERVICE_QNAME = new QName("http://assignment11.tpo.drori.eu/", "FileServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/Task11_SOAP_MTOM/files?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        FILESERVICESERVICE_WSDL_LOCATION = url;
        FILESERVICESERVICE_EXCEPTION = e;
    }

    public FileServiceService() {
        super(__getWsdlLocation(), FILESERVICESERVICE_QNAME);
    }

    public FileServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), FILESERVICESERVICE_QNAME, features);
    }

    public FileServiceService(URL wsdlLocation) {
        super(wsdlLocation, FILESERVICESERVICE_QNAME);
    }

    public FileServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, FILESERVICESERVICE_QNAME, features);
    }

    public FileServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public FileServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns FileService
     */
    @WebEndpoint(name = "FileServicePort")
    public FileService getFileServicePort() {
        return super.getPort(new QName("http://assignment11.tpo.drori.eu/", "FileServicePort"), FileService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns FileService
     */
    @WebEndpoint(name = "FileServicePort")
    public FileService getFileServicePort(WebServiceFeature... features) {
        return super.getPort(new QName("http://assignment11.tpo.drori.eu/", "FileServicePort"), FileService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (FILESERVICESERVICE_EXCEPTION!= null) {
            throw FILESERVICESERVICE_EXCEPTION;
        }
        return FILESERVICESERVICE_WSDL_LOCATION;
    }

}
