
package eu.drori.tpo.assignment11.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.drori.tpo.assignment11.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DownloadFileResponse_QNAME = new QName("http://assignment11.tpo.drori.eu/", "downloadFileResponse");
    private final static QName _GetListOfFilesWithKeyword_QNAME = new QName("http://assignment11.tpo.drori.eu/", "getListOfFilesWithKeyword");
    private final static QName _DownloadFile_QNAME = new QName("http://assignment11.tpo.drori.eu/", "downloadFile");
    private final static QName _UploadNewFile_QNAME = new QName("http://assignment11.tpo.drori.eu/", "uploadNewFile");
    private final static QName _GetListOfFilesWithKeywordResponse_QNAME = new QName("http://assignment11.tpo.drori.eu/", "getListOfFilesWithKeywordResponse");
    private final static QName _UploadNewFileResponse_QNAME = new QName("http://assignment11.tpo.drori.eu/", "uploadNewFileResponse");
    private final static QName _DownloadFileResponseReturn_QNAME = new QName("", "return");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.drori.tpo.assignment11.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DownloadFile }
     * 
     */
    public DownloadFile createDownloadFile() {
        return new DownloadFile();
    }

    /**
     * Create an instance of {@link DownloadFileResponse }
     * 
     */
    public DownloadFileResponse createDownloadFileResponse() {
        return new DownloadFileResponse();
    }

    /**
     * Create an instance of {@link GetListOfFilesWithKeyword }
     * 
     */
    public GetListOfFilesWithKeyword createGetListOfFilesWithKeyword() {
        return new GetListOfFilesWithKeyword();
    }

    /**
     * Create an instance of {@link UploadNewFile }
     * 
     */
    public UploadNewFile createUploadNewFile() {
        return new UploadNewFile();
    }

    /**
     * Create an instance of {@link GetListOfFilesWithKeywordResponse }
     * 
     */
    public GetListOfFilesWithKeywordResponse createGetListOfFilesWithKeywordResponse() {
        return new GetListOfFilesWithKeywordResponse();
    }

    /**
     * Create an instance of {@link UploadNewFileResponse }
     * 
     */
    public UploadNewFileResponse createUploadNewFileResponse() {
        return new UploadNewFileResponse();
    }

    /**
     * Create an instance of {@link FileElement }
     * 
     */
    public FileElement createFileElement() {
        return new FileElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DownloadFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment11.tpo.drori.eu/", name = "downloadFileResponse")
    public JAXBElement<DownloadFileResponse> createDownloadFileResponse(DownloadFileResponse value) {
        return new JAXBElement<DownloadFileResponse>(_DownloadFileResponse_QNAME, DownloadFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfFilesWithKeyword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment11.tpo.drori.eu/", name = "getListOfFilesWithKeyword")
    public JAXBElement<GetListOfFilesWithKeyword> createGetListOfFilesWithKeyword(GetListOfFilesWithKeyword value) {
        return new JAXBElement<GetListOfFilesWithKeyword>(_GetListOfFilesWithKeyword_QNAME, GetListOfFilesWithKeyword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DownloadFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment11.tpo.drori.eu/", name = "downloadFile")
    public JAXBElement<DownloadFile> createDownloadFile(DownloadFile value) {
        return new JAXBElement<DownloadFile>(_DownloadFile_QNAME, DownloadFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadNewFile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment11.tpo.drori.eu/", name = "uploadNewFile")
    public JAXBElement<UploadNewFile> createUploadNewFile(UploadNewFile value) {
        return new JAXBElement<UploadNewFile>(_UploadNewFile_QNAME, UploadNewFile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetListOfFilesWithKeywordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment11.tpo.drori.eu/", name = "getListOfFilesWithKeywordResponse")
    public JAXBElement<GetListOfFilesWithKeywordResponse> createGetListOfFilesWithKeywordResponse(GetListOfFilesWithKeywordResponse value) {
        return new JAXBElement<GetListOfFilesWithKeywordResponse>(_GetListOfFilesWithKeywordResponse_QNAME, GetListOfFilesWithKeywordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UploadNewFileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment11.tpo.drori.eu/", name = "uploadNewFileResponse")
    public JAXBElement<UploadNewFileResponse> createUploadNewFileResponse(UploadNewFileResponse value) {
        return new JAXBElement<UploadNewFileResponse>(_UploadNewFileResponse_QNAME, UploadNewFileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "return", scope = DownloadFileResponse.class)
    public JAXBElement<byte[]> createDownloadFileResponseReturn(byte[] value) {
        return new JAXBElement<byte[]>(_DownloadFileResponseReturn_QNAME, byte[].class, DownloadFileResponse.class, ((byte[]) value));
    }

}
