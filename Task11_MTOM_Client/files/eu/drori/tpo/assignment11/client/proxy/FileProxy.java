package eu.drori.tpo.assignment11.client.proxy;

import java.util.List;

import com.sun.xml.ws.developer.WSBindingProvider;

import eu.drori.tpo.assignment11.client.FileElement;
import eu.drori.tpo.assignment11.client.FileService;
import eu.drori.tpo.assignment11.client.FileServiceService;
import pl.edu.pjwstk.tpr.jaxws.client.common.ProxyBase;

public class FileProxy extends ProxyBase implements FileService {
	private FileServiceService _service;
	private FileService _port;
	
	@Override
	public byte[] downloadFile(String arg0) {
		// TODO Auto-generated method stub
		return getPort().downloadFile(arg0);
	}

	@Override
	public boolean uploadNewFile(FileElement arg0, List<String> arg1) {
		// TODO Auto-generated method stub
		return getPort().uploadNewFile(arg0, arg1);
	}

	@Override
	public List<FileElement> getListOfFilesWithKeyword(String arg0) {
		// TODO Auto-generated method stub
		return getPort().getListOfFilesWithKeyword(arg0);
	}

	@Override
	protected WSBindingProvider getBindingProvider() {
		// TODO Auto-generated method stub
		return (WSBindingProvider)getPort();
	}
	
	private FileService getPort() {
		if (_service == null) {
			_service = new FileServiceService();
		}
		if (_port == null) {
			_port = _service.getFileServicePort();
		}
		return _port;
	}
	

}
