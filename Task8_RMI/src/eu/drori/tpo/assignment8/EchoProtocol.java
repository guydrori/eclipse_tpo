package eu.drori.tpo.assignment8;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface EchoProtocol extends Remote {
	EchoResponse echo(EchoRequest request) throws RemoteException;
}
