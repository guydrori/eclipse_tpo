package eu.drori.tpo.assignment8.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import eu.drori.tpo.assignment8.Configuration;
import eu.drori.tpo.assignment8.implementations.RemoteAddProtocol;
import eu.drori.tpo.assignment8.implementations.RemoteEchoProtocol;

public final class Server {
	
	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(Configuration.PORT);
			Naming.bind(Configuration.ECHO_PROTOCOL_BIND_STRING, RemoteEchoProtocol.getInstance());
			Naming.bind(Configuration.ADD_PROTOCOL_BIND_STRING, RemoteAddProtocol.getInstance());
			System.out.println("Server started");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
