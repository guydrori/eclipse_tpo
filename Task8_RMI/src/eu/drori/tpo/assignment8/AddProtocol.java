package eu.drori.tpo.assignment8;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface AddProtocol extends Remote {
	AddResponse calculateSum(AddRequest request) throws RemoteException;
}
