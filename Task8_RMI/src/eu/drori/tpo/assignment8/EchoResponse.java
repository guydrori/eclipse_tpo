package eu.drori.tpo.assignment8;

import java.io.Serializable;

public interface EchoResponse extends Serializable {
	String getMessage();
}
