package eu.drori.tpo.assignment8.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import eu.drori.tpo.assignment8.AddProtocol;
import eu.drori.tpo.assignment8.AddRequest;
import eu.drori.tpo.assignment8.AddResponse;
import eu.drori.tpo.assignment8.Configuration;
import eu.drori.tpo.assignment8.EchoProtocol;
import eu.drori.tpo.assignment8.EchoRequest;
import eu.drori.tpo.assignment8.EchoResponse;
import eu.drori.tpo.assignment8.implementations.RemoteAddRequest;
import eu.drori.tpo.assignment8.implementations.RemoteEchoRequest;

public final class Client {
	
	public static void main(String[] args) {
		try {
			Registry registry = LocateRegistry.getRegistry(Configuration.PORT);
			AddRequest addRequest = new RemoteAddRequest(2, 2);
			AddProtocol addProtocol = (AddProtocol)registry.lookup(Configuration.ADD_PROTOCOL_BIND_STRING);
			AddResponse addResponse = addProtocol.calculateSum(addRequest);
			System.out.println("Sum of 2 + 2 = " + addResponse.getSum());
			EchoRequest echoRequest = new RemoteEchoRequest();
			EchoProtocol echoProtocol = (EchoProtocol)registry.lookup(Configuration.ECHO_PROTOCOL_BIND_STRING);
			echoRequest.setMessage("ECHO TEST!");
			System.out.println("Local: " + echoRequest.getMessage());
			EchoResponse echoResponse = echoProtocol.echo(echoRequest);
			System.out.println("Remote: " + echoResponse.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
