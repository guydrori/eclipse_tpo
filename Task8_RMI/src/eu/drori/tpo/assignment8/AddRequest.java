package eu.drori.tpo.assignment8;

import java.io.Serializable;

public interface AddRequest extends Serializable {
	void setParameters(int param1, int param2);
	int[] getParameters();
}
