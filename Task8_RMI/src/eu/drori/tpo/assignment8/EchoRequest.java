package eu.drori.tpo.assignment8;

import java.io.Serializable;

public interface EchoRequest extends Serializable {
	void setMessage(String string);
	String getMessage();
}
