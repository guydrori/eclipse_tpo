package eu.drori.tpo.assignment8.implementations;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import eu.drori.tpo.assignment8.AddProtocol;
import eu.drori.tpo.assignment8.AddRequest;
import eu.drori.tpo.assignment8.AddResponse;

public class RemoteAddProtocol extends UnicastRemoteObject implements AddProtocol {

	private static RemoteAddProtocol _singleton;
	private  RemoteAddProtocol() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public AddResponse calculateSum(AddRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		int[] params = request.getParameters();
		int sum = params[0] + params[1];
		RemoteAddResult result = new RemoteAddResult(sum);
		return result;
	}
	
	public static RemoteAddProtocol getInstance() throws RemoteException {
		if (_singleton == null) _singleton = new RemoteAddProtocol();
		return _singleton;
	}
	
}
