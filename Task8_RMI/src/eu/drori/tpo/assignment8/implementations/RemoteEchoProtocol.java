package eu.drori.tpo.assignment8.implementations;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import eu.drori.tpo.assignment8.EchoProtocol;
import eu.drori.tpo.assignment8.EchoRequest;
import eu.drori.tpo.assignment8.EchoResponse;

public class RemoteEchoProtocol extends UnicastRemoteObject implements EchoProtocol {

	private static RemoteEchoProtocol _singleton;
	private RemoteEchoProtocol() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public EchoResponse echo(EchoRequest request) throws RemoteException {
		// TODO Auto-generated method stub
		RemoteEchoResponse response = new RemoteEchoResponse(request.getMessage());
		return response;
	}
	
	public static RemoteEchoProtocol getInstance() throws RemoteException {
		if (_singleton == null) _singleton = new RemoteEchoProtocol();
		return _singleton;
	}

}
