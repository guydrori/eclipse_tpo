package eu.drori.tpo.assignment8.implementations;

import eu.drori.tpo.assignment8.EchoResponse;

public class RemoteEchoResponse implements EchoResponse {

	private String _message;
	
	public RemoteEchoResponse(String message)  {
		System.out.println("ECHO: " + message);
		_message = message;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage()  {
		// TODO Auto-generated method stub
		return _message;
	}

}
