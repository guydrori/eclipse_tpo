package eu.drori.tpo.assignment8.implementations;


import eu.drori.tpo.assignment8.EchoRequest;

public class RemoteEchoRequest implements EchoRequest {

	private String _message;
	public RemoteEchoRequest() {
		_message = "";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setMessage(String message) {
		// TODO Auto-generated method stub
		_message = message;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return _message;
	}

}
