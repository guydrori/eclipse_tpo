package eu.drori.tpo.assignment8.implementations;

import eu.drori.tpo.assignment8.AddResponse;

public class RemoteAddResult implements AddResponse {

	private int _sum;
	public RemoteAddResult(int sum)  {
		_sum = sum;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public int getSum() {
		// TODO Auto-generated method stub
		return _sum;
	}
	
}
