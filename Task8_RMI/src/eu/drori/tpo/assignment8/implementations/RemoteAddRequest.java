package eu.drori.tpo.assignment8.implementations;

import eu.drori.tpo.assignment8.AddRequest;

public class RemoteAddRequest implements AddRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int _param1;
	private int _param2;
	
	public RemoteAddRequest() {
		_param1 = 0;
		_param2 = 0;
	}
	
	public RemoteAddRequest(int param1, int param2) {
		_param1 = param1;
		_param2 = param2;
	}

	@Override
	public void setParameters(int param1, int param2) {
		// TODO Auto-generated method stub
		_param1 = param1;
		_param2 = param2;
	}

	@Override
	public int[] getParameters() {
		// TODO Auto-generated method stub
		return new int[]{_param1,_param2};
	}
}
