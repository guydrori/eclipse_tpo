package eu.drori.tpo.assignment8;

public final class Configuration {
	private Configuration() {
		
	}
	public final static int PORT = 1099;
	public final static String ECHO_PROTOCOL_BIND_STRING = "ECHO_PROTOCOL";
	public final static String ADD_PROTOCOL_BIND_STRING = "ADD_PROTOCOL";
}
