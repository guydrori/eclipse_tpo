package eu.drori.tpo.assignment8;

import java.io.Serializable;

public interface AddResponse extends Serializable {
	int getSum();
}
