package eu.drori.tpo.assignment5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

public final class UserModel {
	
	public static int getUserID(String username,DataSource dataSource) {
		Connection dbConn = null;
		try {
			dbConn = dataSource.getConnection();
			PreparedStatement prepStatement = dbConn.prepareStatement("SELECT \"User_ID\" "
					+ "FROM data.\"User\" WHERE \"Username\"=?");
			prepStatement.setString(1, username);
			ResultSet resultSet = prepStatement.executeQuery();
			if (!resultSet.isBeforeFirst()) return -1;
			resultSet.next();
			return resultSet.getInt(1);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		} finally {
			try {
				dbConn.close();
			} catch (Exception e) {
				e.printStackTrace();
				return -1;
			}
		}
	}
}
