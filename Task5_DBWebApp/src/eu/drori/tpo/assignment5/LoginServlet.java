package eu.drori.tpo.assignment5;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import eu.drori.tpo.assignment5.auth.AuthenticationStatus;
import eu.drori.tpo.assignment5.auth.Authenticator;

@WebServlet({"/login","/logout"})
public class LoginServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String HTML_HEADER = "<html>\n<head>\n<link rel=\"stylesheet\" type=\"text/css\""
			+ " href=\"/Task5_DBWebApp/css/login.css\">\n<title>Guy's Web App | Login</title>\n</head>";
	private static final String HTML_FOOTER = "</body>\n</html>";
	private static final String WELCOME_STRING = "Welcome to<br>Guy's database application";
	
	@Resource(name="jdbc/pgsql_db")
	private DataSource _dataSource;
	
	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie[] cookies = arg0.getCookies();
		boolean isLoggedIn = false;
		if (cookies != null) {
			for (Cookie c: cookies) {
				if (c.getName().equals("userid")) {
					isLoggedIn = true;
					break;
				}
			}
		}
		if (isLoggedIn && !arg0.getRequestURL().toString().contains("/logout")) {
			arg1.sendRedirect("/Task5_DBWebApp/resources");
		} else {
			PrintWriter pw = arg1.getWriter();
			pw.print(HTML_HEADER);
			pw.println("<body>");
			pw.print("    <div class=\"container\">\n    <h1 class=\"welcome text-center\">" 
			+ WELCOME_STRING + "</h1>\n        <div class=\"card card-container\">\n        "
					+ "<h2 class='login_title text-center'>Login</h2>\n        <hr>\n\n            "
					+ "<form class=\"form-signin\" method=post>\n                "
					+ "<span id=\"reauth-user\" class=\"reauth-user\"></span>\n                "
					+ "<p class=\"input_title\">Username</p>\n                "
					+ "<input type=\"text\" id=\"inputUsername\" name=\"inputUsername\" class=\"login_box\" required autofocus>\n                "
					+ "<p class=\"input_title\">Password</p>\n                "
					+ "<input type=\"password\" id=\"inputPassword\" name=\"inputPassword\" class=\"login_box\" required>\n                "
					+ "<div id=\"remember\" class=\"checkbox\">\n                    "
					+ "<label>\n                        \n                    </label>\n                "
					+ "</div>\n                "
					+ "<button class=\"btn btn-lg btn-primary\" type=\"submit\">Login</button>\n            "
					+ "</form><!-- /form -->\n        </div><!-- /card-container -->\n    "
					+ "</div><!-- /container -->");
			if (arg0.getMethod().equals("POST")) {
				AuthenticationStatus authStatus = Authenticator.authenticate(arg0.getParameter("inputUsername"),
						arg0.getParameter("inputPassword"),_dataSource);
				switch (authStatus) {
				case SUCCESS: 
					int userid = UserModel.getUserID(arg0.getParameter("inputUsername"),_dataSource);
					if (userid != -1) {
						Cookie cookie = new Cookie("userid", String.valueOf(userid));
						cookie.setMaxAge(60*10);
						arg1.addCookie(cookie);
						arg1.sendRedirect("/Task5_DBWebApp/resources");
					}
					break;
				case WRONG_PASSWORD: 
					pw.println("<p class=\"error-text\"> Wrong password!</p>");
					break;
				case NO_SUCH_USER:
					pw.println("<p class=\"error-text\"> No user exists with the given username</p>");
				default:
					break;
				}
			}
			if (arg0.getRequestURL().toString().contains("/logout")) {
				try {
					cookies = arg0.getCookies();
					for (Cookie c: cookies) {
						if (c.getName().equals("userid")) {
							c.setMaxAge(0);
							arg1.addCookie(c);
							arg0.getSession().invalidate();
							pw.println("<p class=\"logout-text\">Successfully logged out</p>");
							break;
						}
					}
				} catch (Exception e) {}
			}
			if (arg0.getParameter("req") != null) {
				if (arg0.getParameter("req").equals("true")) {
					pw.println("<p class=\"error-text\"> You must log in!</p>");
				}
			}
			pw.println(HTML_FOOTER);
			pw.close();
		}
	}

}
