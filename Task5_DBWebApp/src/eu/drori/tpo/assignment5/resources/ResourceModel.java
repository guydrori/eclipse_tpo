package eu.drori.tpo.assignment5.resources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public class ResourceModel {
	private DataSource _dataSource;
	private static DataSource _staticDataSource;
	
	public ResourceModel(DataSource dataSource) {
		_dataSource = dataSource;
		if (_staticDataSource == null) _staticDataSource = dataSource;
	}
	
	public Resource getSingleResource(int id) {
		Connection dbConn = null;
		Resource res = null;
		try {
			dbConn = _dataSource.getConnection();
			PreparedStatement prepStatement = dbConn.prepareStatement
					("SELECT \"Name\", \"Content\" FROM data.\"Resource\" "
							+ "WHERE \"ResourceID\" = ?");
			prepStatement.setInt(1, id);
			ResultSet resultSet = prepStatement.executeQuery();
			if (resultSet.isBeforeFirst()) {
				resultSet.next();
				res = new Resource(id, resultSet.getString(1), resultSet.getString(2));
			}
			resultSet.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				dbConn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	
	public static boolean canAccessResource(int resourceId, String userId) {
		Connection dbConn = null;
		int useridInt = Integer.parseInt(userId);
		boolean result = false;
		try {
			dbConn = _staticDataSource.getConnection();
			PreparedStatement prepStatement = dbConn.prepareStatement
					("SELECT R.\"ResourceID\", RA.\"User_ID\""
							+ " FROM data.\"Resource\" R, data.\"Resource Access\" RA"
							+ " WHERE R.\"ResourceID\" = RA.\"ResourceID\" and RA.\"User_ID\" = ?"
							+ " and R.\"ResourceID\" = ?;");
			prepStatement.setInt(1, useridInt);
			prepStatement.setInt(2, resourceId);
			ResultSet resultSet = prepStatement.executeQuery();
			if (resultSet.next()) result = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				dbConn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public List<Resource> getAllAccessibleResourcesForUser(String userid) {
		Connection dbConn = null;
		List<Resource> resList = null;
		int useridInt = Integer.parseInt(userid);
		try {
			dbConn = _dataSource.getConnection();
			PreparedStatement prepStatement = dbConn.prepareStatement
					("SELECT R.\"ResourceID\", R.\"Name\", R.\"Content\""
							+ " FROM data.\"Resource\" R, data.\"Resource Access\" RA"
							+ " WHERE R.\"ResourceID\" = RA.\"ResourceID\" and RA.\"User_ID\" = ?;");
			prepStatement.setInt(1, useridInt);
			ResultSet resultSet = prepStatement.executeQuery();
			if (resultSet.isBeforeFirst()) {
				resList = new ArrayList<>();
				while (resultSet.next()) {
					int resID = resultSet.getInt(1);
					String resName = resultSet.getString(2);
					String resContent = resultSet.getString(3);
					resList.add(new Resource(resID, resName, resContent));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				dbConn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return resList;
	}
}
