package eu.drori.tpo.assignment5.resources;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/resources/view/*")
public class ResourceView extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String HTML_HEADER = "<html>\n<head>\n"
			+ "<meta charset=\"utf-8\">\n<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
			+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n<title>Guy's Web App | Resources</title>"
			+ "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\""
			+ " integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n"
			+ "\n<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css\" integrity=\"sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp\" crossorigin=\"anonymous\">\n"
			+ "<link rel=\"stylesheet\" href=\"/Task5_DBWebApp/css/resources.css\"/>\n"
			+ "</head>";
	private static final String HTML_FOOTER = "</body>\n</html>";
	
	private String getUserId(HttpServletRequest req) {
		Cookie userIdCookie = null;
		Cookie[] cookies = req.getCookies();
		for (Cookie c: cookies) {
			if (c.getName().equals("userid")) {
				userIdCookie = c;
				break;
			}
		}
		return userIdCookie.getValue();
	}
	
	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter printWriter = arg1.getWriter();
		printWriter.println(HTML_HEADER);
		printWriter.println("<body>");
		if (arg0.getSession().getAttribute("resourceList") != null) createResourceListPage(arg0, printWriter);
		else if (arg0.getSession().getAttribute("resource") != null) {
			Resource res = (Resource) arg0.getSession().getAttribute("resource");
			if (!ResourceModel.canAccessResource(res.getID(),
					getUserId(arg0))) arg1.sendRedirect("/Task5_DBWebApp/resources");
			else createResourceInfoPage(arg0,printWriter);
		}
		printWriter.println("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>");
		printWriter.println("<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\" integrity=\"sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa\" crossorigin=\"anonymous\"></script>");
		printWriter.println(HTML_FOOTER);
		printWriter.close();
	}

	private void createResourceInfoPage(HttpServletRequest req, PrintWriter pw) {
		pw.println("<div class=\"container\">\n\t<div class=\"page-header\">");
		pw.println("\t\t<div class=\"btn-toolbar pull-right\">\n\t\t\t<div class=\"btn-group\">");
		pw.println("\t\t\t\t<a href=\"/Task5_DBWebApp/logout\" class=\"btn btn-danger\""
				+ " role=\"button\" style=\"float: right;\">Logout</a>");
		pw.println("\t\t\t</div>\n\t\t</div>");
		pw.println("\t\t<h1 class =\"central_header\">Resource info</h1>");
		pw.println("\t</div>");
		pw.println("\t<div class=\"panel panel-default\">\n\t\t<div class=\"panel-heading\">");
		pw.println("\t\t\tResource\n\t\t</div>");
		pw.println("\t\t<div class=\"panel-body\">\n\t\t\t<h4 class=\"panel-title\" style=\"font-weight: bold;\">");
		Resource res = (Resource) req.getSession().getAttribute("resource");
		pw.println("\t\t\t\t" + res.getName() +"</h4></br>");
		pw.println("\t\t\t<p style=\"font-size: 14px;\">" + res.getContent() + "</p>");
		pw.println("\t\t\t<a href=\"/Task5_DBWebApp/resources\" class=\"btn btn-primary\"> Back </a>");
		pw.println("\t\t</div>\n\t</div>");
		pw.println("</div>");
		
	}
	
	private void createResourceListPage(HttpServletRequest req, PrintWriter pw) {
		pw.println("<div class=\"container\">\n\t<div class=\"page-header\">");
		pw.println("\t\t<div class=\"btn-toolbar pull-right\">\n\t\t\t<div class=\"btn-group\">");
		pw.println("\t\t\t\t<a href=\"/Task5_DBWebApp/logout\" class=\"btn btn-danger\""
				+ " role=\"button\" style=\"float: right;\">Logout</a>");
		pw.println("\t\t\t</div>\n\t\t</div>");
		pw.println("\t\t<h1 class =\"central_header\">Resource list</h1>");
		pw.println("\t</div>");
		createAndPopulateTable(req, pw);
		pw.println("</div>");
	}
	
	@SuppressWarnings("unchecked")
	private void createAndPopulateTable(HttpServletRequest req, PrintWriter pw) {
		pw.println("\t<table class=\"table table-bordered table-hover\"");
		pw.println("\t\t<thead>\n\t\t\t<tr class=\"table-head\">");
		pw.println("\t\t\t\t<th>Resource name</th>\n\t\t\t</tr>");
		pw.println("\t\t</thead>");
		pw.println("\t\t<tbody>\n\t\t\t<tr>");
		List<Resource> resourceList = (List<Resource>) req.getSession().getAttribute("resourceList");
		resourceList.stream().forEach(r-> {
			pw.println("\t\t\t\t<td><a href=\"/Task5_DBWebApp/resources?id=" + r.getID() +
		"\">" + r.getName() + "</a></td>\n\t\t\t</tr>");
		});
		req.getSession().removeAttribute("resourceList");
		pw.println("\t\t</tbody>\n\t</table");
	}
}
