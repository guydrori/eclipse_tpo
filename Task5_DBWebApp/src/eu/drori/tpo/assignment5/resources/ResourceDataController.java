package eu.drori.tpo.assignment5.resources;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/resources")
public class ResourceDataController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Resource(name="jdbc/pgsql_db")
	private DataSource _dataSource;
	
	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie[] cookies = arg0.getCookies();
		if (cookies == null || cookies.length <= 0) {
			arg1.sendRedirect("/Task5_DBWebApp?req=true");
		} else {
			Cookie userIdCookie = null;
			for (Cookie c: cookies) {
				if (c.getName().equals("userid")) {
					userIdCookie = c;
					break;
				}
			}
			if (userIdCookie == null) arg1.sendRedirect("/Task5_DBWebApp");
			else {
				HttpSession session = arg0.getSession();
				ResourceModel model = new ResourceModel(_dataSource);
				if (arg0.getParameter("id") != null) {
					eu.drori.tpo.assignment5.resources.Resource res = 
							model.getSingleResource(Integer.parseInt(arg0.getParameter("id")));
					session.setAttribute("resource", res);
				} else {
					List<eu.drori.tpo.assignment5.resources.Resource> resList =
							model.getAllAccessibleResourcesForUser(userIdCookie.getValue());
					session.setAttribute("resourceList", resList);
				}
				arg0.getRequestDispatcher("/resources/view/").forward(arg0, arg1);
			}
		}
	}

}
