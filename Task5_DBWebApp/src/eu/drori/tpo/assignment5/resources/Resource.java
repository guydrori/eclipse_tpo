package eu.drori.tpo.assignment5.resources;

public class Resource {
	private int _id;
	private String _name;
	private String _content;
	
	public Resource (int id, String name, String content) {
		_id = id;
		_name = name;
		_content = content;
	}

	public int getID() {
		return _id;
	}
	
	public String getName() {
		return _name;
	}

	public String getContent() {
		return _content;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_content == null) ? 0 : _content.hashCode());
		result = prime * result + _id;
		result = prime * result + ((_name == null) ? 0 : _name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resource other = (Resource) obj;
		if (_content == null) {
			if (other._content != null)
				return false;
		} else if (!_content.equals(other._content))
			return false;
		if (_id != other._id)
			return false;
		if (_name == null) {
			if (other._name != null)
				return false;
		} else if (!_name.equals(other._name))
			return false;
		return true;
	}
	
}
