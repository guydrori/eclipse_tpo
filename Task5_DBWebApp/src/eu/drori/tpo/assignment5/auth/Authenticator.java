package eu.drori.tpo.assignment5.auth;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.sql.DataSource;

public final class Authenticator {

	public static AuthenticationStatus authenticate(String username, String password,DataSource dataSource) {
		Connection dbConn = null;
		try {
			dbConn = dataSource.getConnection();
			PreparedStatement prepStatement = dbConn.prepareStatement("SELECT password,salt FROM data.\"User\""
					+ "WHERE \"Username\" = ?");
			prepStatement.setString(1, username);
			ResultSet resultSet = prepStatement.executeQuery();
			if (!resultSet.isBeforeFirst()) return AuthenticationStatus.NO_SUCH_USER;
			resultSet.next();
			byte[] hashedPassword = resultSet.getBytes(1);
			byte[] salt = resultSet.getBytes(2);
			byte[] test = getEncryptedPassword(password, salt);
			if (Arrays.equals(hashedPassword,test)) return AuthenticationStatus.SUCCESS;
			else return AuthenticationStatus.WRONG_PASSWORD;
		} catch (Exception e) {
			e.printStackTrace();
			return AuthenticationStatus.ERROR;
		} finally {
			try {
				dbConn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return AuthenticationStatus.ERROR;
			}
		}
	}
	
	private static byte[] getEncryptedPassword(String password, byte[] salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        // PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
        // specifically names SHA-1 as an acceptable hashing algorithm for PBKDF2
        String algorithm = "PBKDF2WithHmacSHA1";
        // SHA-1 generates 160 bit hashes, so that's what makes sense here
        int derivedKeyLength = 160;
        // Pick an iteration count that works for you. The NIST recommends at
        // least 1,000 iterations:
        // http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
        // iOS 4.x reportedly uses 10,000:
        // http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
        int iterations = 20000;

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);

        SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

        return f.generateSecret(spec).getEncoded();
    }
}
