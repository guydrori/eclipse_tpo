package eu.drori.tpo.assignment5.auth;

public enum AuthenticationStatus {
	SUCCESS,
	WRONG_PASSWORD,
	ERROR,
	NO_SUCH_USER
}
