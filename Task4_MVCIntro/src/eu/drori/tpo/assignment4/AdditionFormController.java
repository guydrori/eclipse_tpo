package eu.drori.tpo.assignment4;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.drori.tpo.assignment4.interfaces.IModel;
import eu.drori.tpo.assignment4.interfaces.IView;

@WebServlet("/form/")
public class AdditionFormController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final IModel DATA_MODEL = AdditionModel.getInstance();
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (req.getMethod().equals("POST") || !req.getParameterMap().keySet().isEmpty()) {
			DATA_MODEL.processRequest(req);
			req.getSession().setAttribute(IView.SESSION_RESULT_PARAMETER, DATA_MODEL);
			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/form/result/");
			requestDispatcher.forward(req, resp);
		} else {
			PrintWriter pw = resp.getWriter();
			pw.println(AdditionResultView.HTML_HEADER);
			pw.println("<h1>Assignment 4 Form</h1>");
			pw.println("<form>\nNumeric input 1: <br>");
			pw.println("<input type=\"number\" name=\"number1\" value=\"" + "0" + "\"><br><br>");
			pw.println("Numeric input 2: <br>");
			pw.println("<input type=\"number\" name=\"number2\" value=\"" + "0" + "\"><br><br>\n");
			pw.println("<button type=\"submit\" formmethod=\"get\">Submit GET</button>" +
			"\t<button type=\"submit\" formmethod=\"post\">Submit POST</button>\n</form>");
			
			pw.println(AdditionResultView.HTML_FOOTER);
			pw.close();
		}
	}
	
}
