package eu.drori.tpo.assignment4;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.drori.tpo.assignment4.interfaces.IModel;
import eu.drori.tpo.assignment4.interfaces.IView;

@WebServlet("/form/result/*")
public class AdditionResultView extends HttpServlet implements IView {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected static final String HTML_HEADER = "<html>\n<head>\n<title>Test servlet</title>\n</head>"
			+ "\n<body>";
	
	protected static final String HTML_FOOTER = "</body>\n</html>";
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (req.getSession().getAttribute(IView.SESSION_RESULT_PARAMETER) == null) {
			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/form/");
			requestDispatcher.forward(req, resp);
		}
		PrintWriter pw = resp.getWriter();
		IModel model = (IModel)req.getSession().getAttribute(IView.SESSION_RESULT_PARAMETER);
		addMainPageToResponse(model, pw);
		pw.println("<br>");
		switch (model.getStatus()) {
			case OKAY:
				pw.println("Result = " + model.getResult());
				break;
			case INVALID_INPUT:
				pw.println("<p style=\"color: red\">At least one of the entered parameters is not an integer number!</p>");
				break;
			case ERROR:
				pw.println("<p style=\"color: red\">Calculation failed :-(</p>");
				break;
		}
		pw.close();
	}
	
	private void addMainPageToResponse(IModel data, PrintWriter pw) throws IOException {
		pw.println(AdditionResultView.HTML_HEADER);
		String number1 = "0";
		String number2 = "0";
		if (data.getStatus() == ProcessingStatus.OKAY) {
			number1 = String.valueOf(data.getParameter(0));
			number2 = String.valueOf(data.getParameter(1));
		}
		
		pw.println("<h1>Assignment 4 Form</h1>");
		pw.println("<form>\nNumeric input 1: <br>");
		pw.println("<input type=\"number\" name=\"number1\" value=\"" + number1 + "\"><br><br>");
		pw.println("Numeric input 2: <br>");
		pw.println("<input type=\"number\" name=\"number2\" value=\"" + number2 + "\"><br><br>\n");
		pw.println("<button type=\"submit\" formmethod=\"get\">Submit GET</button>" +
		"\t<button type=\"submit\" formmethod=\"post\">Submit POST</button>\n</form>");
		
		pw.println(AdditionResultView.HTML_FOOTER);
	}

}
