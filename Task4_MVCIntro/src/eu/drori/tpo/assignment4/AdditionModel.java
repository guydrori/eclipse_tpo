package eu.drori.tpo.assignment4;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import eu.drori.tpo.assignment4.interfaces.IModel;

public class AdditionModel implements IModel{
	private int _int1;
	private int _int2; 
	private int _sum;
	private ProcessingStatus _status;
	
	private static AdditionModel _singleton;
	private static final String DIGIT_PATTERN_STRING = "\\d+";
	
	private static final Pattern DIGIT_PATTERN = Pattern.compile(DIGIT_PATTERN_STRING);

	private AdditionModel() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public ProcessingStatus getStatus() {
		// TODO Auto-generated method stub
		return _status;
	}

	@Override
	public Object getResult() {
		// TODO Auto-generated method stub
		return _sum;
	}

	@Override
	public Object getParameter(int index) {
		// TODO Auto-generated method stub
		if (index == 0) return _int1;
		else if (index == 1) return _int2;
		else throw new IllegalArgumentException("This model only has two parameters");
	}

	@Override
	public IModel processRequest(HttpServletRequest req) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try {
			String number1 = req.getParameter("number1");
			String number2 = req.getParameter("number2");
			if (DIGIT_PATTERN.matcher(number1).matches() && DIGIT_PATTERN.matcher(number2).matches()) {
				_int1 = Integer.parseInt(number1);
				_int2 = Integer.parseInt(number2);
				_sum = _int1 + _int2;
				_status = ProcessingStatus.OKAY;
			} else {
				_status = ProcessingStatus.INVALID_INPUT;
			}
		} catch (Exception e) {
			_status = ProcessingStatus.ERROR;
		}
		return this;
	}
	
	public static AdditionModel getInstance() {
		if (_singleton == null) {
			_singleton = new AdditionModel();
		}
		return _singleton;
	}

}
