package eu.drori.tpo.assignment4;

public enum ProcessingStatus {
	OKAY,
	ERROR,
	INVALID_INPUT
}
