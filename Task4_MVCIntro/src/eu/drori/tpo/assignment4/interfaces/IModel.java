package eu.drori.tpo.assignment4.interfaces;

import javax.servlet.http.HttpServletRequest;

import eu.drori.tpo.assignment4.ProcessingStatus;

public interface IModel {
	public IModel processRequest(HttpServletRequest req);
	public ProcessingStatus getStatus();
	public Object getResult();
	public Object getParameter(int index);
}
