package eu.drori.tpo.assignment13.interfaces;

import java.util.List;

import eu.drori.tpo.assignment13.common.Person;

public interface People {
	List<Person> getPeopleBornOnDate(String birthdate);
	List<Person> getPeopleWithSurname(String surname);
}
