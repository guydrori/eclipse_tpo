package eu.drori.tpo.assignment13.proxy;

import java.util.List;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import eu.drori.tpo.assignment13.common.Person;
import eu.drori.tpo.assignment13.interfaces.People;

public class PeopleProxy extends ProxyBase implements People{

	public PeopleProxy() {
		super("http://localhost:8080/Task13_REST_2/people");
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Person> getPeopleBornOnDate(String birthdate) {
		// TODO Auto-generated method stub
		WebTarget method = path("sortbydate");
		final GenericType<List<Person>> type = new GenericType<List<Person>>() {};
		List<Person> response = method.path(birthdate)
				.request(MediaType.APPLICATION_JSON)
				.get(type);
		return response;
	}

	@Override
	public List<Person> getPeopleWithSurname(String surname) {
		// TODO Auto-generated method stub
		WebTarget method = path("sortbysurname");
		final GenericType<List<Person>> type = new GenericType<List<Person>>() {};
		List<Person> response = method.path(surname)
				.request(MediaType.APPLICATION_JSON)
				.get(type);
		return response;
	}

}
