package eu.drori.tpo.assignment13.common;


import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Person implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String surname;
	private String dateOfBirth;
	
	public Person(String firstName, String surname, String dateOfBirth) {
		if (firstName == null || surname == null || dateOfBirth == null) throw new NullPointerException();
		this.firstName = firstName;
		this.surname = surname;
		this.dateOfBirth = dateOfBirth;
	}
	
	public Person() {
		
	}
	
	@XmlElement
	public String getFirstName() {
		return firstName;
	}
	
	@XmlElement
	public String getSurname() {
		return surname;
	}
	
	@XmlElement
	public String getDateOfBirth() {
		return dateOfBirth;
	}
}
