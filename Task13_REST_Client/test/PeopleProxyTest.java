import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.drori.tpo.assignment13.common.Person;
import eu.drori.tpo.assignment13.proxy.PeopleProxy;

public class PeopleProxyTest {
	private PeopleProxy _proxy;
	
	@Before
	public void before() {
		_proxy = new PeopleProxy();
	}
	
	@Test
	public void getPeopleBornOnDate() {
		List<Person> peopleList = _proxy.getPeopleBornOnDate("10-05-1978");
		Assert.assertEquals(2, peopleList.size());
		Assert.assertEquals("10-05-1978",peopleList.get(0).getDateOfBirth());
		Assert.assertEquals("10-05-1978",peopleList.get(1).getDateOfBirth());
	}
	
	@Test
	public void getPeopleWithSurname() {
		List<Person> peopleList = _proxy.getPeopleWithSurname("Noodle");
		Assert.assertEquals(1, peopleList.size());
		Assert.assertEquals("Noodle", peopleList.get(0).getSurname());
	}
	
}
