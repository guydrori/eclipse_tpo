package eu.drori.tpo.assignment13;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class Person implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String _firstName;
	private String _surname;
	private LocalDate _dateOfBirth;
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-YYYY");
	private static Map<LocalDate, List<Person>> dateMap = new HashMap<>();
	private static Map<String, List<Person>> surnameMap = new HashMap<>();
	
	public Person(String firstName, String surname, LocalDate dateOfBirth) {
		if (firstName == null || surname == null || dateOfBirth == null) throw new NullPointerException();
		_firstName = firstName;
		_surname = surname;
		_dateOfBirth = dateOfBirth;
		addPersonToDateMap(this);
		addPersonToSurnameMap(this);
	}
	
	@XmlElement
	public String getFirstName() {
		return _firstName;
	}
	
	@XmlElement
	public String getSurname() {
		return _surname;
	}
	
	@XmlElement
	public String getDateOfBirth() {
		return FORMATTER.format(_dateOfBirth);
	}
	
	protected LocalDate getDateOfBirthAsLocalDate() {
		return _dateOfBirth;
	}
	
	private void addPersonToDateMap(Person person) {
		LocalDate dateOfBirth = person.getDateOfBirthAsLocalDate();
		if (dateMap.containsKey(dateOfBirth)) {
			List<Person> listOfPersonsBornOnThatDate = dateMap.get(dateOfBirth);
			listOfPersonsBornOnThatDate.add(person);
			dateMap.put(dateOfBirth, listOfPersonsBornOnThatDate);
		} else {
			List<Person> listOfPersonsBornOnThatDate = new ArrayList<Person>();
			listOfPersonsBornOnThatDate.add(person);
			dateMap.put(dateOfBirth, listOfPersonsBornOnThatDate);
		}
	}
	
	private static void addPersonToSurnameMap(Person person) {
		String surname = person.getSurname();
		if (surnameMap.containsKey(surname)) {
			List<Person> listOfPersonsWithThisSurname = surnameMap.get(surname);
			listOfPersonsWithThisSurname.add(person);
			surnameMap.put(surname, listOfPersonsWithThisSurname);
		} else {
			List<Person> listOfPersonsWithThisSurname = new ArrayList<Person>();
			listOfPersonsWithThisSurname.add(person);
			surnameMap.put(surname, listOfPersonsWithThisSurname);
		}
	}
	
	public static List<Person> getListOfPeopleBornOnDate(LocalDate localDate) {
		if (dateMap.containsKey(localDate)) {
			return dateMap.get(localDate);
		} else {
			return null;
		}
	}
	
	public static List<Person> getListOfPeopleWithSurname(String surname) {
		if (surnameMap.containsKey(surname)) {
			return surnameMap.get(surname);
		} else {
			return null;
		}
	}
}
