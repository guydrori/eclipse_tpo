package eu.drori.tpo.assignment13.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

@WebServlet( //
urlPatterns = "/*",//
initParams = { //
		@WebInitParam(name = "jersey.config.server.provider.packages", value = "eu.drori.tpo.assignment13"), //
		@WebInitParam(name = "jersey.config.server.provider.scanning.recursive", value = "true"), //
		@WebInitParam(name = "org.glassfish.jersey.api.json.POJOMappingFeature", value = "true")//
}, //
asyncSupported = true)
public class WebServicePort extends ServletContainer {

	public void init() throws ServletException {
		super.init();
		final Application application = new ResourceConfig().packages(
				"eu.drori.tpo.assignment13").register(JacksonFeature.class);
		/* final Application application2 = new ResourceConfig().packages(
				"eu.drori.tpo.assignment13").register(MoxyJsonFeature.class); */
	}
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.service(req, res);
		System.out.println("Served request");
	}


	private static final long serialVersionUID = -6946589644571530717L;
}