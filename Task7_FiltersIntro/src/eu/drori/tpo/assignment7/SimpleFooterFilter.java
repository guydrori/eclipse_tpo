package eu.drori.tpo.assignment7;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName="footerFilter",urlPatterns = "/index.html")
public class SimpleFooterFilter implements Filter {

	private static final String FOOTER_STRING = "\n<div id=\"date\"></div>\n</body>\n</html>";
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		ResponseWrapper wrapper = new ResponseWrapper((HttpServletResponse) arg1);
		arg2.doFilter(arg0, wrapper);
		String nextChainStep = wrapper.getStringWriter().toString();
		PrintWriter pw = arg1.getWriter();
		pw.println(nextChainStep);
		pw.print(FOOTER_STRING);
		pw.close();
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
}
