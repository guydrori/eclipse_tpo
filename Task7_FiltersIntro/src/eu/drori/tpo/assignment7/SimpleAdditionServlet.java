package eu.drori.tpo.assignment7;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/add/*")
public class SimpleAdditionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest arg0, HttpServletResponse arg1) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (!arg0.getParameterMap().keySet().isEmpty()) {
			String number1String = arg0.getParameter("number1");
			String number2String = arg0.getParameter("number2");
			int number1 = Integer.parseInt(number1String);
			int number2 = Integer.parseInt(number2String);
			int sum = number1 + number2;
			PrintWriter pw = arg1.getWriter();
			pw.println("{\"sum\": " + sum + " }");
			pw.close();
		} else {
			super.service(arg0, arg1);
		}
	}
}
