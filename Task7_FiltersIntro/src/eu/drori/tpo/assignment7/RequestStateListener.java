package eu.drori.tpo.assignment7;

import java.util.Map;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class RequestStateListener implements ServletRequestListener {

	@Override
	public void requestDestroyed(ServletRequestEvent arg0) {
		// TODO Auto-generated method stub
		System.err.println("Request destroyed");
	}

	@Override
	public void requestInitialized(ServletRequestEvent arg0) {
		// TODO Auto-generated method stub
		System.err.println("Request initialized. Contents:");
		Map<String,String[]> parameterMap = arg0.getServletRequest().getParameterMap();
		parameterMap.entrySet().forEach(e->{
			String arrayString = "[";
			for (String s: e.getValue()) {
				arrayString = arrayString.concat(s +",");
			}
			arrayString += "]";
			System.err.println(e.getKey() +": " + arrayString);
		});
	}

}
