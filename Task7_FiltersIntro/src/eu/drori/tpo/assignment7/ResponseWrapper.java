package eu.drori.tpo.assignment7;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class ResponseWrapper extends HttpServletResponseWrapper {

	private StringWriter _stringWriter = null;
	
	public ResponseWrapper(HttpServletResponse response) {
		super(response);
		// TODO Auto-generated constructor stub
	}
	
	public PrintWriter getWriter() throws IOException {
		if (_stringWriter == null) _stringWriter = new StringWriter();
		return new PrintWriter(_stringWriter);
	}
	
	public ServletOutputStream getOutputStream() throws IOException {
		throw new IllegalStateException("getOutputStream() not alllowed for ResponseWrapper");
	}
	
	public StringWriter getStringWriter() {
		return _stringWriter;
	}
}
