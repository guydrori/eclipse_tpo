package eu.drori.tpo.assignment7;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter(filterName="headerFilter",urlPatterns="/index.html")
public class SimpleHeaderFilter implements Filter {

	private static final String HEADER_STRING = "<html>\n<head>"
			+ "\n\t<title>Test servlet</title>\n</head>\nTask 7</body>";
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		PrintWriter pw = arg1.getWriter();
		pw.println(HEADER_STRING);
		arg2.doFilter(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
