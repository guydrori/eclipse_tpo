
function callback(response) {
	$("#sum").html("<br>Sum= ".concat(response.sum));
}

function ajaxPost() {
	$.post("add/", 
			{
				number1: $("#number1").val(),
				number2: $("#number2").val()
			},
			callback,
			"json"
	);
}

function ajaxGet() {
	$.get("add/", 
			{
				number1: $("#number1").val(),
				number2: $("#number2").val()
			},
			callback,
			"json"
	);
}

$("#number1").focusout(ajaxPost);
$("#number2").focusout(ajaxPost);

function setDate(response) {
	$("#date").html("<p>".concat(response.date).concat("</p>"));
}

var interval = 1000;
function refreshDate() {
	$.get("date/",null,setDate,"json");
	setTimeout(refreshDate,interval);
}
setTimeout(refreshDate,interval);