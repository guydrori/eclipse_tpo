package pl.edu.pjwstk.tpr.jaxrs.chunked;
	

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.glassfish.jersey.server.ChunkedOutput;

import pl.edu.pjwstk.tpr.jaxrs.WebServiceBase;
import pl.edu.pjwstk.tpr.jaxrs.chunked.tasks.ReplyChunkedTask;

/**
 * @author Edgar G�owacki <edgar@glowacki.eu>
 */

@Path("/chunked")
public class SampleChunked extends WebServiceBase {

	@GET
	@Path("/reply-chunked/{request}")
	public ChunkedOutput<String> replyChunked(@PathParam("request") String request) {
		final ChunkedOutput<String> output = new ChunkedOutput<String>(String.class);
		ReplyChunkedTask task = new ReplyChunkedTask(output, request);
		submit(task);
		return output;
	}
}