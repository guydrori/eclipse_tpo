package pl.edu.pjwstk.tpr.jaxrs.chunked.tasks;

import org.glassfish.jersey.server.ChunkedOutput;

import pl.edu.pjwstk.tpr.jaxrs.TaskBase;
import pl.edu.pjwstk.tpr.jaxrs.TprException;

public final class ReplyChunkedTask extends TaskBase<String> {

	private static final int DELAY = 2000;
	private static final int RESPONSE_COUNT = 20;

	private final String _request;
	private final ChunkedOutput<String> _output;

	public ReplyChunkedTask(ChunkedOutput<String> output, String request) {
		_request = request;
		_output = output;
	}

	public void run() {
		for (int i = 0; i < RESPONSE_COUNT; i++) {
			sleep(DELAY);
			String response = "RESPONSE " + i + ": " + _request;
			write(response);
		}
		close();
	}

	private void write(String message) {
		try {
			_output.write(message);
		} catch (Throwable ex) {
			throw new TprException("error occured", ex);
		} finally {
			close();
		}
	}

	private void close() {
		try {
			if (_output != null && !_output.isClosed()) {
				_output.close();
			}
		} catch (Throwable ex) {
			throw new TprException("error occured", ex);
		}
	}
}