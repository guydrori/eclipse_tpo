package eu.drori.tpo.assignment13;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/people")
public class People extends WebServiceBase {
	
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-uuuu");
	
	static {
		new Person("Avril","Leverett",LocalDate.of(1978, 5, 10));
		new Person("Tobie","Noodle",LocalDate.of(1997, 9, 10));
		new Person("Frederica","Spedroni",LocalDate.of(1967, 2, 17));
		new Person("Russell","Burke",LocalDate.of(1978,5,10));
	}
	
	@GET
	@Path("/reply/{request}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Person> getPeopleBornOnDate(@PathParam("request") String birthDate) {
		try {
			LocalDate date  = LocalDate.parse(birthDate, FORMATTER);
			List<Person> peopleBornOnThisDate = Person.getListOfPeopleBornOnDate(date);
			return peopleBornOnThisDate;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/* @GET
	@Path("/reply/{request}")
	@Produces("application/json")
	public Person[] getPeopleWithSurname(@PathParam("request") String surname) {
		List<Person> peopleWithThisSurname = Person.getListOfPeopleWithSurname(surname);
		if (peopleWithThisSurname != null) {
			Person[] personArray = new Person[peopleWithThisSurname.size()];
			return peopleWithThisSurname.toArray(personArray);
		} else {
			return null;
		}
	} */
}
