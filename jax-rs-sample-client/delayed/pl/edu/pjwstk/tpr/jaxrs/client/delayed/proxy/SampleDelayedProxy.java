package pl.edu.pjwstk.tpr.jaxrs.client.delayed.proxy;

import java.util.concurrent.Future;

import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import pl.edu.pjwstk.tpr.jaxrs.client.ProxyBase;
import pl.edu.pjwstk.tpr.jaxrs.client.delayed.SampleDelayed;

public class SampleDelayedProxy extends ProxyBase implements SampleDelayed {
	
	private static final String BASE_URI = "http://localhost:8080/jax-rs-sample/sample";
	
	public SampleDelayedProxy() {
		super(BASE_URI);
	}
	
	public Future<String> replyDelayed(String request, InvocationCallback<String> callback) {
		WebTarget method = getMethod("reply");
		return method
		    .path(request)
	        .request(MediaType.TEXT_PLAIN_TYPE)
	        .async()
	        .get(callback);
	}
}