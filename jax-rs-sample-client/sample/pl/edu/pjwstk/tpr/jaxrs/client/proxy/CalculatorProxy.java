package pl.edu.pjwstk.tpr.jaxrs.client.proxy;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import pl.edu.pjwstk.tpr.jaxrs.client.Calculator;
import pl.edu.pjwstk.tpr.jaxrs.client.ProxyBase;

public class CalculatorProxy extends ProxyBase implements Calculator {
	
	private static final String BASE_URI = "http://localhost:8080/jax-rs-sample/calculator";
	
	public CalculatorProxy() {
		super(BASE_URI);
	}

	public int add(int component1, int component2) {
		WebTarget method = getMethod("add");
		int sum = method
		    .queryParam("component1", "" + component1)
		    .queryParam("component2", "" + component2)
		    .request(MediaType.APPLICATION_JSON_TYPE)
		    .get(Integer.TYPE);
		return sum;
	}

	public int subtract(int minuend, int subtrahend) {
		WebTarget method = getMethod("subtract");
		int difference = method
		    .queryParam("minuend", "" + minuend)
		    .queryParam("subtrahend", "" + subtrahend)
		    .request(MediaType.APPLICATION_JSON_TYPE)
		    .get(Integer.TYPE);
		return difference;
	}
}