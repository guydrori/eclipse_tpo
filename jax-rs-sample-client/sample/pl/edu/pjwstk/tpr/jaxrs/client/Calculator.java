package pl.edu.pjwstk.tpr.jaxrs.client;

public interface Calculator {

	int add(int component1, int component2);
	
	int subtract(int minuend, int subtrahend);
}