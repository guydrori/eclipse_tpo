package pl.edu.pjwstk.tpr.jaxrs.client;

public interface Sample {

	String reply(String request);
}