package pl.edu.pjwstk.tpr.jaxrs.client.proxy;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import pl.edu.pjwstk.tpr.jaxrs.client.ProxyBase;
import pl.edu.pjwstk.tpr.jaxrs.client.Sample;

public class SampleProxy extends ProxyBase implements Sample {
	
	private static final String BASE_URI = "http://localhost:8080/jax-rs-sample/sample";
	
	public SampleProxy() {
		super(BASE_URI);
	}
	
	public String reply(String request) {
		WebTarget method = getMethod("reply");
		String response = method
		    .path(request)
	        .request(MediaType.TEXT_PLAIN_TYPE)
	        .get(String.class);
		return response;
	}
}