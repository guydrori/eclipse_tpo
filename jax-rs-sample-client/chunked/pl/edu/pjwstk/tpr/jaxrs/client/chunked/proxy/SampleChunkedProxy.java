package pl.edu.pjwstk.tpr.jaxrs.client.chunked.proxy;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ChunkedInput;

import pl.edu.pjwstk.tpr.jaxrs.client.ProxyBase;
import pl.edu.pjwstk.tpr.jaxrs.client.chunked.SampleChunked;

public class SampleChunkedProxy extends ProxyBase implements SampleChunked {
	
	private static final String BASE_URI = "http://localhost:8080/jax-rs-sample/chunked";

	public SampleChunkedProxy() {
		super(BASE_URI);
	}

	public ChunkedInput<String> replyChunked(String request) {
		WebTarget method = getMethod("reply-chunked");
		Response response = method
		    .path(request)
		    .request()
		    .get();
		ChunkedInput<String> input = response.readEntity(new GenericType<ChunkedInput<String>>(){});
		return input;
	}
}