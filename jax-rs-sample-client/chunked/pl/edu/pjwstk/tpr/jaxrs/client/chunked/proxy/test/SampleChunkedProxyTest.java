package pl.edu.pjwstk.tpr.jaxrs.client.chunked.proxy.test;

import org.glassfish.jersey.client.ChunkedInput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.edu.pjwstk.tpr.jaxrs.client.chunked.SampleChunked;
import pl.edu.pjwstk.tpr.jaxrs.client.chunked.proxy.SampleChunkedProxy;

public class SampleChunkedProxyTest {
	
	private SampleChunked _sut;
	
	@Before
	public void before() {
		_sut = new SampleChunkedProxy();
	}

	@Test
	public void replyChunked() {
		final String request = "request";
		ChunkedInput<String> chunks = _sut.replyChunked(request);
		String chunk;
		int counter = 0;
		while (!chunks.isClosed() && (chunk = chunks.read()) != null) {
			String expected = "RESPONSE " + counter + ": " + request;
			Assert.assertEquals(expected, chunk);
		}
	}
}