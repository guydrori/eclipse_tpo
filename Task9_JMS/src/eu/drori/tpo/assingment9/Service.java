package eu.drori.tpo.assingment9;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.ScheduledMessage;

import eu.drori.tpo.assignment9.messages.ArithmeticOperation;
import eu.drori.tpo.assignment9.messages.ArithmeticOperationRequest;
import eu.drori.tpo.assignment9.messages.ArithmeticOperationResponse;
import eu.drori.tpo.assignment9.messages.RandomGenerationRequest;
import eu.drori.tpo.assignment9.messages.RandomGeneratorResponse;
import eu.drori.tpo.assignment9.messages.Response;

import java.util.Random;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Service implements MessageListener {

	private static final Logger LOGGER = Logger.getLogger(Service.class.getName());
	private static QueueConnection _conn;
	private static QueueSender _sender2;
	private static QueueSender _sender3;
	private static QueueSender _sender4;
	private static final Random RANDOM = new Random();
	private String _identifier;
	private static int _counter = 0;
	private static QueueSession _jmsSession;
	
	public Service() {
		++_counter;
		_identifier = "SERVICE-" + _counter;
		LOGGER.fine(_identifier + ": Listening for messages");
	}
	
	public static void main(String[] args) {
		try {
			InitialContext jndi = new InitialContext();
			ActiveMQConnectionFactory queueConnFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
			queueConnFactory.setTrustAllPackages(true);
			_conn = queueConnFactory.createQueueConnection();
			_jmsSession = _conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			LOGGER.setLevel(Level.ALL);
			ConsoleHandler consoleHandler = new ConsoleHandler();
			consoleHandler.setFormatter(new SimpleFormatter());
			consoleHandler.setLevel(Level.ALL);
			LOGGER.addHandler(consoleHandler);
			_sender2 = _jmsSession.createSender((Queue)jndi.lookup("Queue2"));
			_sender3 = _jmsSession.createSender((Queue)jndi.lookup("Queue3"));
			_sender4 = _jmsSession.createSender((Queue)jndi.lookup("Queue4"));
			for (int i = 0; i < 5; ++i) {
				QueueReceiver queueReceiver = _jmsSession.createReceiver((Queue)jndi.lookup("Queue"));
				queueReceiver.setMessageListener(new Service());
			}
			Thread exitInputListenerThread = new Thread(()->{
				try {
					System.out.println("Enter EXIT to close the service");
					Scanner scanner = new Scanner(System.in);
					String input = scanner.nextLine();
					if (input.equals("EXIT")) {
						scanner.close();
						Service.closeEverything();
						System.exit(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					Service.closeEverything();
				}
			});
			_conn.start();
			exitInputListenerThread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void onMessage(Message arg0) {
		// TODO Auto-generated method stub
		if (arg0 instanceof TextMessage) {
			try {
				LOGGER.info(_identifier + ": message received: " + ((TextMessage)arg0).getText());
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		else if (arg0 instanceof ObjectMessage) {
			try  {
				LOGGER.info(_identifier + ": message received");
				ObjectMessage objectMessage = (ObjectMessage)arg0;
				Object contents = objectMessage.getObject();
				if (contents instanceof RandomGenerationRequest) {
					RandomGenerationRequest request = (RandomGenerationRequest)contents;
					RandomGeneratorResponse response = new RandomGeneratorResponse(request, RANDOM.nextInt());
					ObjectMessage message = _jmsSession.createObjectMessage(response);
					message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, generateDelay());
					QueueSender sender = getSender(request.getSenderID());
					sender.send(message);
					LOGGER.info(_identifier + ": message sent");
				}
				if (contents instanceof ArithmeticOperationRequest) {
					ArithmeticOperationRequest request = (ArithmeticOperationRequest)contents;
					Response response = processArithmeticOperation(request);
					ObjectMessage message = _jmsSession.createObjectMessage(response);
					message.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, generateDelay());
					QueueSender sender = getSender(request.getSenderID());
					sender.send(message);
					LOGGER.info(_identifier + ": message sent");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private ArithmeticOperationResponse processArithmeticOperation(ArithmeticOperationRequest request) {
		ArithmeticOperation operationType = request.getType();
		int result = 0;
		int[] parameters = request.getParameters();
		switch (operationType) {
		case ADDITION:
			result = parameters[0] + parameters[1];
			break;
		case SUBTRACTION:
			result = parameters[0] + parameters[1];
			break;
		case MULTIPLICATION:
			result = parameters[0] * parameters[1];
			break;
		case DIVISION:
			if (parameters[1] != 0) {
				result = parameters[0] / parameters[1];
				break;
			}
		}
		return new ArithmeticOperationResponse(result, request);
	}
	
	public static void closeEverything() {
		try {
			_sender2.close();
			_sender3.close();
			_sender4.close();
			_jmsSession.close();
			_conn.close();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Error when exiting", e);
		}
	}
	
	private long generateDelay() {
		int threeSeconds = 3000;
		int increment = RANDOM.nextInt(2001);
		int waitPeriod = threeSeconds + increment;
		LOGGER.fine("Delay: "+((long)waitPeriod));
		return (long)waitPeriod;
	}
	
	private QueueSender getSender(int senderID) {
		switch (senderID) {
		case 2:
			return _sender2;
		case 3:
			return _sender3;
		case 4:
			return _sender4;
		}
		return null;
	}

}
