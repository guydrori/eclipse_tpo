package eu.drori.tpo.assingment9;

public class Assignment9Exception extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Assignment9Exception() {
		// TODO Auto-generated constructor stub
	}

	public Assignment9Exception(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public Assignment9Exception(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public Assignment9Exception(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public Assignment9Exception(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
