package eu.drori.tpo.assingment9;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.activemq.ActiveMQConnectionFactory;

import eu.drori.tpo.assignment9.messages.ArithmeticOperation;
import eu.drori.tpo.assignment9.messages.ArithmeticOperationRequest;
import eu.drori.tpo.assignment9.messages.ArithmeticOperationResponse;
import eu.drori.tpo.assignment9.messages.RandomGenerationRequest;
import eu.drori.tpo.assignment9.messages.RandomGeneratorResponse;
import eu.drori.tpo.assignment9.messages.Request;

public class Producer implements MessageListener {

	private static final Logger LOGGER = Logger.getLogger(Producer.class.getName());
	private java.util.Queue<Request> _requestQueue = new ArrayBlockingQueue<>(10);
	private static final Random RANDOM = new Random();
	private static int _counter = 0;
	private String _identifier;
	private static QueueConnection _conn;
	private static QueueSession _session;
	private static QueueSender _sender;
	private int _intID;
	public Producer(int intID) {
		++_counter;
		_identifier ="PRODUCER-" + _counter;
		_intID = intID;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			InitialContext jndi = new InitialContext();
			ActiveMQConnectionFactory queueConnFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
			queueConnFactory.setTrustAllPackages(true);
			_conn = queueConnFactory.createQueueConnection();
			_session = _conn.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			ConsoleHandler consoleHandler = new ConsoleHandler();
			consoleHandler.setFormatter(new SimpleFormatter());
			consoleHandler.setLevel(Level.ALL);
			LOGGER.addHandler(consoleHandler);
			LOGGER.setLevel(Level.ALL);
			_sender = _session.createSender((Queue)jndi.lookup("Queue"));
			_conn.start();
			createProducers();
			Thread exitInputListenerThread = new Thread(()->{
				try {
					System.out.println("Enter EXIT to close the producer");
					Scanner scanner = new Scanner(System.in);
					String input = scanner.nextLine();
					if (input.equals("EXIT")) {
						scanner.close();
						Producer.closeEverything();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					Producer.closeEverything();
				}
			});
			//_conn.start();
			exitInputListenerThread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void sendAGeneratedRequest() {
		try {
			Request request = generateRandomRequest();
			ObjectMessage requestMessage = _session.createObjectMessage(request);
			_requestQueue.add(request);
			//Thread.sleep(1000);
			_sender.send(requestMessage);
			LOGGER.log(Level.INFO, _identifier + ": message sent");
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, _identifier,e);
		}
	}
	
	public static void closeEverything() {
		try {
			_sender.close();
			_conn.stop();
			_session.close();
			_conn.close();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE,"Error when closing",e);
		}
		System.exit(0);
	}
	
	@Override
	public void onMessage(Message arg0) {
		// TODO Auto-generated method stub
		if (arg0 instanceof ObjectMessage) {
			 try {
				 Object contents = ((ObjectMessage)arg0).getObject();
				 if (contents instanceof RandomGeneratorResponse) {
					 RandomGeneratorResponse response = (RandomGeneratorResponse)contents;
					 RandomGenerationRequest originalRequest = response.getOriginalRequest();
					 if (_requestQueue.contains(originalRequest)) {
						 _requestQueue.remove(originalRequest);
						 LOGGER.log(Level.INFO,_identifier + ": received random number: " + response.getRandomNumber());
						 Thread.sleep(500);
						 sendAGeneratedRequest();
					 }
				 }
				 if (contents instanceof ArithmeticOperationResponse) {
					 ArithmeticOperationResponse response = (ArithmeticOperationResponse)contents;
					 ArithmeticOperationRequest originalRequest = response.getOriginalRequest();
					 if (_requestQueue.contains(originalRequest)) {
						 _requestQueue.remove(originalRequest);
						 LOGGER.log(Level.INFO,_identifier + ": received result " + response.getResult());
						 Thread.sleep(5000);
						 sendAGeneratedRequest();
					 }
				 }
			 } catch (Exception e) {
				 e.printStackTrace();
			 }
			
		}
	}
	
	private Request generateRandomRequest() {
		int requestType = RANDOM.nextInt(2);
		Request request = null;
		switch (requestType) {
		case 0:
			request = new RandomGenerationRequest(_intID);
			break;
		case 1:
			ArithmeticOperation[] arithmeticOpeartions = ArithmeticOperation.values();
			int index = RANDOM.nextInt(arithmeticOpeartions.length);
			request = new ArithmeticOperationRequest(arithmeticOpeartions[index], new int[]{RANDOM.nextInt(),RANDOM.nextInt()},_intID);
		}
		return request;
	}
	
	private static void createProducers() {
		try {
			InitialContext jndi = new InitialContext();
			Producer producer = new Producer(2);
			QueueReceiver queueReceiver = _session.createReceiver((Queue)jndi.lookup("Queue2"));
			queueReceiver.setMessageListener(producer);
			Thread.sleep(500);
			producer.sendAGeneratedRequest();
			Producer producer2 = new Producer(3);
			QueueReceiver queueReceiver2 = _session.createReceiver((Queue)jndi.lookup("Queue3"));
			queueReceiver2.setMessageListener(producer2);
			Thread.sleep(500);
			producer2.sendAGeneratedRequest();
			Producer producer3 = new Producer(4);
			QueueReceiver queueReceiver3 = _session.createReceiver((Queue)jndi.lookup("Queue4"));
			queueReceiver3.setMessageListener(producer3);
			Thread.sleep(500);
			producer3.sendAGeneratedRequest();
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception when creating producers", e);
		}
	}

}
