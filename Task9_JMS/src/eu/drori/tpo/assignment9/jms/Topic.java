package eu.drori.tpo.assignment9.jms;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.region.DestinationStatistics;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.store.TopicMessageStore;
import org.apache.activemq.thread.TaskRunnerFactory;

public class Topic extends org.apache.activemq.broker.region.Topic {

	public Topic(BrokerService brokerService, ActiveMQDestination destination, TopicMessageStore store,
			DestinationStatistics parentStats, TaskRunnerFactory taskFactory) throws Exception {
		super(brokerService, destination, store, parentStats, taskFactory);
		// TODO Auto-generated constructor stub
	}

}
