package eu.drori.tpo.assignment9.messages;

public enum ArithmeticOperation {
	ADDITION,
	MULTIPLICATION,
	SUBTRACTION,
	DIVISION
}
