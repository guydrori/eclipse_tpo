package eu.drori.tpo.assignment9.messages;

public class ArithmeticOperationResponse implements Response {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int _result;
	private ArithmeticOperationRequest _originalRequest;
	
	public ArithmeticOperationResponse (int result, ArithmeticOperationRequest originalRequest) {
		if (originalRequest == null) throw new IllegalArgumentException("The original request cannot be null");
		_result = result;
		_originalRequest = originalRequest;
	}
	
	public int getResult() {
		return _result;
	}
	
	public ArithmeticOperationRequest getOriginalRequest() {
		return _originalRequest;
	}
	
}
