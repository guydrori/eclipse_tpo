package eu.drori.tpo.assignment9.messages;

import java.time.LocalDateTime;
import java.util.Arrays;

public class ArithmeticOperationRequest implements Request {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArithmeticOperation _type;
	private int[] _parameters;
	private LocalDateTime _creationDateTime;
	private int _sender;
	
	public ArithmeticOperationRequest(ArithmeticOperation type, int[] parameters, int senderNo) {
		if (parameters.length != 2) throw new IllegalArgumentException("The parameter array must have exactly 2 elements");
		_type = type;
		_parameters = parameters;
		_sender = senderNo;
		_creationDateTime = LocalDateTime.now();
	}
	
	public ArithmeticOperationRequest(ArithmeticOperation type, int param1, int param2, int senderNo) {
		_type = type;
		_parameters = new int[]{param1,param2};
		_creationDateTime = LocalDateTime.now();
		_sender = senderNo;
	}
	
	public ArithmeticOperation getType() {
		return _type;
	}
	public int[] getParameters() {
		return _parameters;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_creationDateTime == null) ? 0 : _creationDateTime.hashCode());
		result = prime * result + Arrays.hashCode(_parameters);
		result = prime * result + ((_type == null) ? 0 : _type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArithmeticOperationRequest other = (ArithmeticOperationRequest) obj;
		if (_creationDateTime == null) {
			if (other._creationDateTime != null)
				return false;
		} else if (!_creationDateTime.equals(other._creationDateTime))
			return false;
		if (!Arrays.equals(_parameters, other._parameters))
			return false;
		if (_type != other._type)
			return false;
		return true;
	}

	@Override
	public int getSenderID() {
		// TODO Auto-generated method stub
		return _sender;
	}
	
}
