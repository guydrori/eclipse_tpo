package eu.drori.tpo.assignment9.messages;

import java.time.LocalDateTime;

public class RandomGenerationRequest implements Request {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private LocalDateTime _creationTime;
	private int _senderID;
	public RandomGenerationRequest(int senderID) {
		_creationTime = LocalDateTime.now();
		_senderID = senderID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_creationTime == null) ? 0 : _creationTime.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RandomGenerationRequest other = (RandomGenerationRequest) obj;
		if (_creationTime == null) {
			if (other._creationTime != null)
				return false;
		} else if (!_creationTime.equals(other._creationTime))
			return false;
		return true;
	}
	@Override
	public int getSenderID() {
		// TODO Auto-generated method stub
		return _senderID;
	}
	
	
}
