package eu.drori.tpo.assignment9.messages;


public class RandomGeneratorResponse implements Response {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int _randomNumber;
	private RandomGenerationRequest _request;
	
	public RandomGeneratorResponse(RandomGenerationRequest request, int randomNumber) {
		_randomNumber = randomNumber;
		_request = request;
	}
	
	public int getRandomNumber() {
		return _randomNumber;
	}
	
	public RandomGenerationRequest getOriginalRequest() {
		return _request;
	}
}
