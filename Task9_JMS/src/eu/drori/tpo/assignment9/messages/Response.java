package eu.drori.tpo.assignment9.messages;

import java.io.Serializable;

public interface Response extends Serializable {
	Request getOriginalRequest();
}
