package eu.drori.tpo.assignment9.messages;

import java.io.Serializable;

public interface Request extends Serializable {
	public int getSenderID();
}
