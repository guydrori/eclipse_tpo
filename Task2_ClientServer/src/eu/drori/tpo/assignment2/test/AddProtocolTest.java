package eu.drori.tpo.assignment2.test;

import org.junit.Assert;
import org.junit.Test;

import eu.drori.tpo.assignment2.common.AddProtocol;
import eu.drori.tpo.assignment2.common.AddResponse;

public class AddProtocolTest {
	
	@Test
	public void processRequest() {
		AddResponse response = (AddResponse) AddProtocol.getInstance()
				.processRequest(AddProtocol.ADD_REQUEST + " 26 16");
		Assert.assertEquals(new AddResponse(26,16), response);
	}
	
	@Test
	public void deserializeResponse() {
		AddResponse response = (AddResponse) AddProtocol.getInstance()
				.deseralizeResponse(AddProtocol.ADD_RESPONSE + " 29");
		Assert.assertEquals(new AddResponse(29), response);
	}
}
