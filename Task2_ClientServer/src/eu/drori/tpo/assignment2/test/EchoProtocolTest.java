package eu.drori.tpo.assignment2.test;

import org.junit.Assert;
import org.junit.Test;

import eu.drori.tpo.assignment2.common.EchoProtocol;
import eu.drori.tpo.assignment2.common.EchoResponse;

public class EchoProtocolTest {
	
	@Test
	public void processRequest() {
		EchoResponse response = (EchoResponse) EchoProtocol.getInstance()
				.processRequest(EchoProtocol.ECHO_REQUEST_STRING + " abcdefghijklmnop");
		Assert.assertEquals(new EchoResponse("abcdefghijklmnop"), response);
	}
	
	@Test
	public void deserializeResponse() {
		EchoResponse response = (EchoResponse) EchoProtocol.getInstance()
				.deseralizeResponse(EchoProtocol.ECHO_RESPONSE_STRING + " Hello!");
		Assert.assertEquals(new EchoResponse("Hello!"), response);
	}
}
