package eu.drori.tpo.assignment2.common;

import eu.drori.tpo.assignment2.common.interfaces.IResponse;

public class AddResponse implements IResponse {
	
	private int _sum;
	
	public AddResponse(int int1, int int2) {
		_sum = int1 + int2;
	}
	
	public AddResponse(int[] params) {
		_sum = params[0] + params[1];
	}
	
	public AddResponse(int sum) {
		_sum = sum;
	}
	
	@Override
	public String getResponse() {
		// TODO Auto-generated method stub
		return AddProtocol.ADD_RESPONSE + " " + _sum;
	}
	
	@Override
	public String toString() {
		return "Sum = " + _sum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + _sum;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddResponse other = (AddResponse) obj;
		if (_sum != other._sum)
			return false;
		return true;
	}
}
