package eu.drori.tpo.assignment2.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.drori.tpo.assignment2.common.interfaces.IProtocol;
import eu.drori.tpo.assignment2.common.interfaces.IResponse;

public class EchoProtocol implements IProtocol {

	private static EchoProtocol _singleton; 
	
	private EchoProtocol() {
		
	}

	public static final String ECHO_REQUEST_STRING = "ECHO";
	public static final String ECHO_RESPONSE_STRING = "ECHO_REPLY";
	
	private static final String _echoRequestPatternString = "(" + ECHO_REQUEST_STRING + "\\s)(.*)";
	public static final Pattern _echoRequestPattern = Pattern.compile(_echoRequestPatternString);
	
	private static final String _echoResponsePatternString = "(" + ECHO_RESPONSE_STRING + "\\s)(.*)";
	private static final Pattern ECHO_RESPONSE_PATTERN = Pattern.compile(_echoResponsePatternString);
	
	
	@Override
	public IResponse processRequest(String request) {
		Matcher matcher = _echoRequestPattern.matcher(request);
		if (matcher.matches()) {
			EchoRequest echoRequest = new EchoRequest(matcher.group(2));
			return new EchoResponse(echoRequest);
		} else {
			throw new Assignment02Exception();
		}
	}

	@Override
	public IResponse deseralizeResponse(String response) {
		// TODO Auto-generated method stub
		Matcher matcher = ECHO_RESPONSE_PATTERN.matcher(response);
		if (matcher.matches()) {
			return new EchoResponse(matcher.group(2));
		} else {
			throw new Assignment02Exception();
		}
	}
	
	public static EchoProtocol getInstance() {
		if (_singleton == null) {
			_singleton = new EchoProtocol();
			return _singleton;
		}
		return _singleton;
	}
}
