package eu.drori.tpo.assignment2.common;

public class Assignment02Exception extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Assignment02Exception() {
		// TODO Auto-generated constructor stub
	}

	public Assignment02Exception(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public Assignment02Exception(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public Assignment02Exception(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public Assignment02Exception(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
