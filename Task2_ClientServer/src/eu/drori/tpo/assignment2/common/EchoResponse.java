package eu.drori.tpo.assignment2.common;

import eu.drori.tpo.assignment2.common.interfaces.IResponse;

public class EchoResponse implements IResponse {
	private String _message;
	
	public EchoResponse(String message) {
		_message = message;
	}
	
	public EchoResponse(EchoRequest request) {
		_message = request.getMessage();
	}
	
	public String getMessage() {
		return _message;
	}

	@Override
	public String getResponse() {
		// TODO Auto-generated method stub
		return EchoProtocol.ECHO_RESPONSE_STRING +" " + _message;
	}
	
	@Override
	public String toString() {
		return _message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((_message == null) ? 0 : _message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EchoResponse other = (EchoResponse) obj;
		if (_message == null) {
			if (other._message != null)
				return false;
		} else if (!_message.equals(other._message))
			return false;
		return true;
	}
	
}
