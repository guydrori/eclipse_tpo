package eu.drori.tpo.assignment2.common;

import eu.drori.tpo.assignment2.common.interfaces.IRequest;

public class AddRequest implements IRequest {
	
	private int _int1;
	private int _int2;
	
	public AddRequest(int int1, int int2) {
		_int1 = int1;
		_int2 = int2;
	}
	
	public int[] getParams() {
		return new int[]{_int1,_int2};
	}
	
	@Override
	public String getRequest() {
		// TODO Auto-generated method stub
		return AddProtocol.ADD_REQUEST + " " + _int1 + " " + _int2;
	}

}
