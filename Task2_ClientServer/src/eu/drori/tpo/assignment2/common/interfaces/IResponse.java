package eu.drori.tpo.assignment2.common.interfaces;

public interface IResponse {
	public String getResponse();
}
