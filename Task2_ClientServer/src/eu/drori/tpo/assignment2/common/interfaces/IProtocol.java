package eu.drori.tpo.assignment2.common.interfaces;

public interface IProtocol {
	public abstract IResponse processRequest(String request);
	public abstract IResponse deseralizeResponse(String response);
}
