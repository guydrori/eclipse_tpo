package eu.drori.tpo.assignment2.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.drori.tpo.assignment2.common.interfaces.IProtocol;
import eu.drori.tpo.assignment2.common.interfaces.IResponse;

public class AddProtocol implements IProtocol{

	private static AddProtocol _singleton;
	
	public static final String ADD_REQUEST = "ADD";
	private static final String ADD_REQUEST_PATTERN_STRING = "(" + ADD_REQUEST + ")\\s(\\d+)\\s(\\d+)";
	public static final Pattern ADD_REQUEST_PATTERN = Pattern.compile(ADD_REQUEST_PATTERN_STRING);
	
	public static final String ADD_RESPONSE = "ADD_RESULT";
	private static final String ADD_RESPONSE_PATTERN_STRING = "(" + ADD_RESPONSE + ")\\s(\\d+)";
	public static final Pattern ADD_RESPONSE_PATTERN = Pattern.compile(ADD_RESPONSE_PATTERN_STRING);
	
	private AddProtocol() {
		// TODO Auto-generated constructor stub
	}
	
	public static AddProtocol getInstance() {
		if (_singleton == null) {
			_singleton = new AddProtocol();
		}
		return _singleton;
	}
	
	@Override
	public IResponse processRequest(String request) {
		// TODO Auto-generated method stub
		Matcher requestMatcher = ADD_REQUEST_PATTERN.matcher(request);
		if (requestMatcher.matches()) {
			AddRequest addRequest = new AddRequest(Integer.parseInt(requestMatcher.group(2)),
					Integer.parseInt(requestMatcher.group(3)));
			return new AddResponse(addRequest.getParams());
		} else {
			throw new Assignment02Exception();
		}
	}

	@Override
	public IResponse deseralizeResponse(String response) {
		// TODO Auto-generated method stub
		Matcher responseMatcher = ADD_RESPONSE_PATTERN.matcher(response);
		if (responseMatcher.matches()) {
			return new AddResponse(Integer.parseInt(responseMatcher.group(2)));
		} else {
			throw new Assignment02Exception();
		}
	}

}
