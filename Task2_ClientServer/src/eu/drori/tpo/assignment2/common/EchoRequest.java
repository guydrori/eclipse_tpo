package eu.drori.tpo.assignment2.common;

import eu.drori.tpo.assignment2.common.interfaces.IRequest;

public class EchoRequest implements IRequest {
	private String _message;
	
	public EchoRequest(String message) {
		_message = message;
	}
	
	public String getMessage() {
		return _message;
	}

	@Override
	public String getRequest() {
		// TODO Auto-generated method stub
		return EchoProtocol.ECHO_REQUEST_STRING + " " + _message;
	}
}
