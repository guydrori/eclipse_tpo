package eu.drori.utp.assignment2.client;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.regex.Matcher;

import eu.drori.tpo.assignment2.common.AddProtocol;
import eu.drori.tpo.assignment2.common.AddRequest;
import eu.drori.tpo.assignment2.common.Assignment02Exception;
import eu.drori.tpo.assignment2.common.EchoProtocol;
import eu.drori.tpo.assignment2.common.EchoRequest;
import eu.drori.tpo.assignment2.common.interfaces.IRequest;
import eu.drori.tpo.assignment2.common.interfaces.IResponse;

public class Client {
	
	private int _bufferSize = 1024;
	private ByteBuffer _byteBuffer = ByteBuffer.allocate(_bufferSize);
	private Charset _utf8Charset = StandardCharsets.UTF_8;
	private SocketChannel _socketCh;
	private boolean run = true;
	public Client(InetSocketAddress isa) {
		System.out.println("Welcome to the client, please follow the following syntax:");
		System.out.println("For add requests: ADD number1 number 2");
		System.out.println("For echo requests: ECHO text");
		System.out.println("To exit enter EXIT");
		try {
			_socketCh = SocketChannel.open();
			_socketCh.connect(isa);
			_socketCh.configureBlocking(false);
			Scanner scanner = new Scanner(System.in);
			while (run) {
				String input = scanner.nextLine();
				parseConsoleInput(input);
			}
			scanner.close();
			System.exit(0);
		} catch (Exception e) {
			throw new Assignment02Exception(e.getMessage());
		}
	}
	
	private void parseConsoleInput(String input) {
		if (input.equals("EXIT")) {
			run = false;
			try {
				_socketCh.close();
				_socketCh.socket().close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (input.contains("ADD")) {
			Matcher addRequestMatcher = AddProtocol.ADD_REQUEST_PATTERN.matcher(input);
			if (addRequestMatcher.matches()) {
				int int1 = Integer.parseInt(addRequestMatcher.group(2));
				int int2 = Integer.parseInt(addRequestMatcher.group(3));
				request(_socketCh,new AddRequest(int1, int2));
			}
		} else if (input.contains("ECHO")) {
			Matcher inputRequestMatcher = EchoProtocol._echoRequestPattern.matcher(input);
			if (inputRequestMatcher.matches()) {
				String message = inputRequestMatcher.group(2);
				request(_socketCh,new EchoRequest(message));
			}
		}
	}
	
	private void request(SocketChannel socketCh, IRequest request) {
		if (!socketCh.isOpen()) return;
		while(!socketCh.isConnected());
		ByteBuffer requestBuffer = _utf8Charset.encode(request.getRequest());
		try {
			socketCh.write(requestBuffer);
			_byteBuffer.clear();
			boolean read = true;
			while (read) {
				int n = socketCh.read(_byteBuffer);
				if (n > 0) {
					_byteBuffer.flip();
					read = false;
				}
			}
			String responseString = _utf8Charset.decode(_byteBuffer).toString();
			IResponse response= null;
			if (responseString.contains(EchoProtocol.ECHO_RESPONSE_STRING)) {
				response = EchoProtocol.getInstance().deseralizeResponse(responseString);
			} else if (responseString.contains(AddProtocol.ADD_REQUEST)) {
				response = AddProtocol.getInstance().deseralizeResponse(responseString);
			}
			System.out.println(response);
		} catch (Exception e) {
			throw new Assignment02Exception(e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		if (args.length == 2) {
			String ip = args[0];
			String portString = args[1];
			int port = -1;
			InetSocketAddress isa = null;
			try {
				port = Integer.parseInt(portString);
				isa = new InetSocketAddress(ip, port);
			} catch (Exception e) {
				throw new Assignment02Exception("Arguments should follow the following format: IP port");
			}
			new Client(isa);
		}
	}
}
