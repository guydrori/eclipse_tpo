package eu.drori.utp.assignment2.server;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import eu.drori.tpo.assignment2.common.AddProtocol;
import eu.drori.tpo.assignment2.common.Assignment02Exception;
import eu.drori.tpo.assignment2.common.EchoProtocol;
import eu.drori.tpo.assignment2.common.interfaces.IResponse;

public class Server {

	private ServerSocketChannel _serverSocketChannel = null;
	private Selector _selector = null;
	private boolean running = true;
	
	private static Charset _utf8Charsest = StandardCharsets.UTF_8;
	private static final int _bufferSize = 1024;
	
	private ByteBuffer _byteBuffer = ByteBuffer.allocate(_bufferSize);
	
	public Server (int port) {
		try {
			_serverSocketChannel = ServerSocketChannel.open();
			_serverSocketChannel.configureBlocking(false);
			_serverSocketChannel.socket().bind(new InetSocketAddress(port));
			_selector = Selector.open();
			_serverSocketChannel.register(_selector,SelectionKey.OP_ACCEPT);
			Thread exitInputListenerThread = new Thread(()->{
				try {
					System.out.println("Enter EXIT to close the server");
					Scanner scanner = new Scanner(System.in);
					String input = scanner.nextLine();
					if (input.equals("EXIT")) {
						running = false;
						scanner.close();
						System.exit(0);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
			exitInputListenerThread.start();
		} catch (Exception e) {
			throw new Assignment02Exception(e.getMessage());
		}
		serviceConnections();
	}
	
	public void serviceConnections() {
		while (running) {
			try {
				_selector.select();
				Set<SelectionKey> selectorKeys = _selector.selectedKeys();
				if (!selectorKeys.isEmpty()) {
					Iterator<SelectionKey> keyIter = selectorKeys.iterator();
					while (keyIter.hasNext()) {
						SelectionKey key = keyIter.next();
						keyIter.remove();
						if (key.isAcceptable()) {
							SocketChannel socketCh = _serverSocketChannel.accept();
							socketCh.configureBlocking(false);
							socketCh.register(_selector, SelectionKey.OP_READ);
							continue;
						} 
						
						if (key.isReadable()) {
							SocketChannel socketCh = (SocketChannel)key.channel();
							serviceRequest(socketCh);
							continue;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			_selector.close();
		} catch (Exception e) {
			throw new Assignment02Exception(e.getMessage());
		}
	}
	
	private void serviceRequest(SocketChannel socketCh) {
		if (!socketCh.isOpen()) return;
		_byteBuffer.clear();
		try {
			boolean read = true;
			while (read) {
				int n = socketCh.read(_byteBuffer);
				if (n == -1) {
					socketCh.close();
					socketCh.socket().close();
					return;
				} else if (n <= 0){
					_byteBuffer.flip();
					read = false;
				}
			}
			String requestString = _utf8Charsest.decode(_byteBuffer).toString();
			if (requestString.contains("ADD")) {
				IResponse response = AddProtocol.getInstance().processRequest(requestString);
				writeResponse(socketCh, response);
			}
			if (requestString.contains("ECHO")) {
				IResponse response = EchoProtocol.getInstance().processRequest(requestString);
				writeResponse(socketCh, response);
			}
		} catch (Exception e) {
			try {
				socketCh.close();
				socketCh.socket().close();
			} catch (Exception ex) {
				
			}
			throw new Assignment02Exception(e.getMessage());
		}
	}
	
	private void writeResponse(SocketChannel socketCh, IResponse response) throws Exception {
		ByteBuffer buffer = _utf8Charsest.encode(response.getResponse());
		socketCh.write(buffer);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length == 1) {
			int port = -100;
			try {
				port = Integer.parseInt(args[0]);
			} catch (Exception e) {
				throw new Assignment02Exception("No port has been given");
			}
			new Server(port);
		} else {
			throw new Assignment02Exception("No port has been given");
		}
	}

}
