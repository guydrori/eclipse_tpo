package pl.edu.pjwstk.tpo.jaxws;

public class TpoException extends Exception {

	private static final long serialVersionUID = -8366710958797049391L;

	public TpoException(String message, Throwable cause) {
		super(message, cause);
	}
}