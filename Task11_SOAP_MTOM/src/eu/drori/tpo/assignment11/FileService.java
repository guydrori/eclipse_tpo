package eu.drori.tpo.assignment11;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import pl.edu.pjwstk.tpo.jaxws.WebServiceBase;

@MTOM
@WebService
public class FileService extends WebServiceBase {

	private static final Path FILE_DIR = Paths.get("files");
	private static List<String> _fileNameList = new ArrayList<>();
	private static Map<String,List<FileElement>> _keywordMap = new HashMap<>();
	
	@WebMethod
	public boolean uploadNewFile(FileElement file, String[] keywords) {
		try {
			if (!Files.exists(FILE_DIR)) {
				Files.createDirectory(FILE_DIR);
			}
			Path filePath = FILE_DIR.resolve(file.getFileName());
			byte[] fileContents = file.getFileContents();
			Files.deleteIfExists(filePath);
			Files.write(filePath, fileContents, StandardOpenOption.CREATE_NEW);
			FileElement fileForStorage = new FileElement(file.getFileName(), file.getFileSize());
			if (keywords != null) {
				for (String s: keywords) {
					if (_keywordMap.containsKey(s)) {
						List<FileElement> fileList = _keywordMap.get(s);
						fileList.add(fileForStorage);
						_keywordMap.put(s, fileList);
					} else {
						List<FileElement> fileList = new ArrayList<>();
						fileList.add(fileForStorage);
						_keywordMap.put(s, fileList);
					}
				}
			}
			_fileNameList.add(file.getFileName());
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@WebMethod
	public FileElement[] getListOfFilesWithKeyword(String keyword) {
		if (_keywordMap.containsKey(keyword)) {
			List<FileElement> fileList = _keywordMap.get(keyword);
			FileElement[] fileArray = new FileElement[fileList.size()];
			return fileList.toArray(fileArray);
		} else {
			return null;
		}
	}
	
	@WebMethod
	public byte[] downloadFile(String fileName) {
		if (_fileNameList.contains(fileName)) {
			Path filePath = FILE_DIR.resolve(fileName);
			try {
				return Files.readAllBytes(filePath);
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		} else {
			return null;
		}
	}
	
}
