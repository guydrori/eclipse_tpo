package eu.drori.tpo.assignment11;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.ws.soap.MTOM;

@XmlAccessorType(XmlAccessType.PROPERTY)
public class FileElement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement
	private String _fileName;
	@XmlElement
	private long _fileSize;
	@XmlElement
	private byte[] _fileContents;
	
	public FileElement() {
		
	}
	
	public FileElement(String fileName, long fileSize) {
		if (fileName == null) throw new NullPointerException();
		_fileName = fileName;
		_fileSize = fileSize;
	}
	
	public FileElement(String fileName, long fileSize, byte[] fileContents) {
		if (fileName == null) throw new NullPointerException();
		_fileName = fileName;
		_fileSize = fileSize;
		_fileContents = fileContents;
	}
	
	@XmlTransient
	public void setFileContents(byte[] fileContents) {
		_fileContents = fileContents;
	}
	
	public byte[] getFileContents() {
		return _fileContents;
	}
	
	public String getFileName() {
		return _fileName;
	}
	
	public long getFileSize() {
		return _fileSize;
	}
	
	@Override
	public String toString() {
		return _fileName + ", " + _fileSize + " bytes";
	}
	
	
}
