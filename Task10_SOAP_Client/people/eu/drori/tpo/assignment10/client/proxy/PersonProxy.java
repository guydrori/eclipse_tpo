package eu.drori.tpo.assignment10.client.proxy;

import java.util.List;

import com.sun.xml.ws.developer.WSBindingProvider;

import eu.drori.tpo.assignment10.client.Person;
import eu.drori.tpo.assignment10.client.PersonService;
import eu.drori.tpo.assignment10.client.PersonServiceService;
import pl.edu.pjwstk.tpr.jaxws.client.common.ProxyBase;

public class PersonProxy extends ProxyBase implements PersonService{
	private PersonServiceService _service;
	private PersonService _port;
	
	@Override
	public List<Person> getPeopleBornOnDate(String birthDate) {
		// TODO Auto-generated method stub
		return getPort().getPeopleBornOnDate(birthDate);
	}
	@Override
	public List<Person> getPeopleWithSurname(String surname) {
		// TODO Auto-generated method stub
		return getPort().getPeopleWithSurname(surname);
	}
	
	@Override
	protected WSBindingProvider getBindingProvider() {
		// TODO Auto-generated method stub
		return (WSBindingProvider)getPort();
	}
	
	private PersonService getPort() {
		if (_service == null) {
			_service = new PersonServiceService();
		}
		if (_port == null) {
			_port = _service.getPersonServicePort();
		}
		return _port;
	}
	
}
