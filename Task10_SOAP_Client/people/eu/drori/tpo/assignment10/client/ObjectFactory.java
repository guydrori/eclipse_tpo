
package eu.drori.tpo.assignment10.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.drori.tpo.assignment10.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetPeopleBornOnDateResponse_QNAME = new QName("http://assignment10.tpo.drori.eu/", "getPeopleBornOnDateResponse");
    private final static QName _GetPeopleWithSurnameResponse_QNAME = new QName("http://assignment10.tpo.drori.eu/", "getPeopleWithSurnameResponse");
    private final static QName _GetPeopleBornOnDate_QNAME = new QName("http://assignment10.tpo.drori.eu/", "getPeopleBornOnDate");
    private final static QName _GetPeopleWithSurname_QNAME = new QName("http://assignment10.tpo.drori.eu/", "getPeopleWithSurname");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.drori.tpo.assignment10.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPeopleWithSurname }
     * 
     */
    public GetPeopleWithSurname createGetPeopleWithSurname() {
        return new GetPeopleWithSurname();
    }

    /**
     * Create an instance of {@link GetPeopleBornOnDate }
     * 
     */
    public GetPeopleBornOnDate createGetPeopleBornOnDate() {
        return new GetPeopleBornOnDate();
    }

    /**
     * Create an instance of {@link GetPeopleWithSurnameResponse }
     * 
     */
    public GetPeopleWithSurnameResponse createGetPeopleWithSurnameResponse() {
        return new GetPeopleWithSurnameResponse();
    }

    /**
     * Create an instance of {@link GetPeopleBornOnDateResponse }
     * 
     */
    public GetPeopleBornOnDateResponse createGetPeopleBornOnDateResponse() {
        return new GetPeopleBornOnDateResponse();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPeopleBornOnDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment10.tpo.drori.eu/", name = "getPeopleBornOnDateResponse")
    public JAXBElement<GetPeopleBornOnDateResponse> createGetPeopleBornOnDateResponse(GetPeopleBornOnDateResponse value) {
        return new JAXBElement<GetPeopleBornOnDateResponse>(_GetPeopleBornOnDateResponse_QNAME, GetPeopleBornOnDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPeopleWithSurnameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment10.tpo.drori.eu/", name = "getPeopleWithSurnameResponse")
    public JAXBElement<GetPeopleWithSurnameResponse> createGetPeopleWithSurnameResponse(GetPeopleWithSurnameResponse value) {
        return new JAXBElement<GetPeopleWithSurnameResponse>(_GetPeopleWithSurnameResponse_QNAME, GetPeopleWithSurnameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPeopleBornOnDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment10.tpo.drori.eu/", name = "getPeopleBornOnDate")
    public JAXBElement<GetPeopleBornOnDate> createGetPeopleBornOnDate(GetPeopleBornOnDate value) {
        return new JAXBElement<GetPeopleBornOnDate>(_GetPeopleBornOnDate_QNAME, GetPeopleBornOnDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPeopleWithSurname }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://assignment10.tpo.drori.eu/", name = "getPeopleWithSurname")
    public JAXBElement<GetPeopleWithSurname> createGetPeopleWithSurname(GetPeopleWithSurname value) {
        return new JAXBElement<GetPeopleWithSurname>(_GetPeopleWithSurname_QNAME, GetPeopleWithSurname.class, null, value);
    }

}
