package eu.drori.tpo.assignment10.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import eu.drori.tpo.assignment10.client.Person;
import eu.drori.tpo.assignment10.client.proxy.PersonProxy;

public class PeopleTest {
	
	private PersonProxy _proxy;
	
	@Before
	public void before() {
		_proxy = new PersonProxy();
	}
	
	@Test
	public void getPeopleWithSurname() {
		List<Person> correctList = new ArrayList<>();
		correctList.add(new Person("Frederica","Spedroni","17-02-1967"));
		List<Person> listFromWebService = _proxy.getPeopleWithSurname("Spedroni");
		Assert.assertEquals(correctList, listFromWebService);
	}
	
	@Test
	public void getPeopleBornOnDate() {
		List<Person> correctList = new ArrayList<>();
		correctList.add(new Person("Avril","Leverett","10-05-1978"));
		correctList.add(new Person("Russell","Burke","10-05-1978"));
		List<Person> listFromWebService = _proxy.getPeopleBornOnDate("10-05-1978");
		Assert.assertEquals(correctList,listFromWebService);
	}
	
}
