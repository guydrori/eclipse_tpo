package eu.drori.tpo.assignment10;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import pl.edu.pjwstk.tpo.jaxws.WebServiceBase;

@WebService
public class PersonService extends WebServiceBase{

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd-MM-uuuu");
	
	static {
		new Person("Avril","Leverett",LocalDate.of(1978, 5, 10));
		new Person("Tobie","Noodle",LocalDate.of(1997, 9, 10));
		new Person("Frederica","Spedroni",LocalDate.of(1967, 2, 17));
		new Person("Russell","Burke",LocalDate.of(1978,5,10));
	}
	
	
	@WebMethod
	public Person[] getPeopleBornOnDate(String birthDate) {
		try {
			LocalDate date  = LocalDate.parse(birthDate, FORMATTER);
			List<Person> peopleBornOnThisDate = Person.getListOfPeopleBornOnDate(date);
			if (peopleBornOnThisDate != null) {
				Person[] personArray = new Person[peopleBornOnThisDate.size()];
				return peopleBornOnThisDate.toArray(personArray);
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@WebMethod
	public Person[] getPeopleWithSurname(String surname) {
		List<Person> peopleWithThisSurname = Person.getListOfPeopleWithSurname(surname);
		if (peopleWithThisSurname != null) {
			Person[] personArray = new Person[peopleWithThisSurname.size()];
			return peopleWithThisSurname.toArray(personArray);
		} else {
			return null;
		}
	}
	
}
