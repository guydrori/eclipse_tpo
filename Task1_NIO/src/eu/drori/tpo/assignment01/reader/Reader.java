package eu.drori.tpo.assignment01.reader;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

import eu.drori.tpo.assignment01.common.Assignment01Exception;
import eu.drori.tpo.assignment01.common.OperationMark;

public class Reader {
	private static RandomAccessFile _file;
	private static MappedByteBuffer _mappedBuff;
	
	public static void main(String[] args) {
		loadFile();
		processFile();
	}
	
	private static void loadFile() {
		File file = new File("tmp.dat");
		if (!file.canRead()) {
			throw new Assignment01Exception("Cannot read from file!");
		}
		try {
			_file = new RandomAccessFile(file, "rw");
			FileChannel channel = _file.getChannel();
			_mappedBuff = channel.map(MapMode.READ_WRITE, 0,50);
		} catch (Exception e) {
			throw new Assignment01Exception("Cannot read from file!");
		}
	}
	
	private static void processFile() {
		while(true) {
			_mappedBuff.rewind();
			if (_mappedBuff.getChar() == OperationMark.Continue.getChar()) {
				if (_mappedBuff.getChar() == OperationMark.Write.getChar()) {
					int int1 = _mappedBuff.getInt();
					int int2 = _mappedBuff.getInt();
					int sum = int1 + int2;
					System.out.println(int1 + " + " + int2 + "= " + sum);
					_mappedBuff.rewind();
					_mappedBuff.putChar(OperationMark.Continue.getChar());
					_mappedBuff.putChar(OperationMark.Read.getChar());
				}
			} else {
				_mappedBuff.rewind();
				if (_mappedBuff.getChar() == OperationMark.Stop.getChar()) {
					break;
				}
			}
		}
	}
}
