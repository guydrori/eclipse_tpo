package eu.drori.tpo.assignment01.common;

public class OperationMark {
	private char _mark;
	public static OperationMark Read = new OperationMark('R');
	public static OperationMark Write = new OperationMark('W');
	public static OperationMark Stop = new OperationMark('S');
	public static OperationMark Continue = new OperationMark('C');
	protected OperationMark(char mark) {
		_mark = mark;
	}
	
	public char getChar() {
		return _mark;
	}
}
