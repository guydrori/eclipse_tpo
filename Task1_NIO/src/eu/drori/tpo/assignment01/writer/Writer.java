package eu.drori.tpo.assignment01.writer;

import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.Random;
import java.util.Scanner;

import eu.drori.tpo.assignment01.common.Assignment01Exception;
import eu.drori.tpo.assignment01.common.OperationMark;

import java.io.File;

public class Writer {
	
	private static RandomAccessFile _file;
	private static MappedByteBuffer _mappedBuff;
	private static Random _random = new Random();
	private static volatile boolean _stop = false;
	
	public static void main(String[] args) {
		Thread writerThread = new Thread(()->{
			loadFile();
			operateOnFile();
		});
		writerThread.start();
		Scanner scanner = new Scanner(System.in);
		while (!scanner.next().equals("STOP") && !scanner.next().equals("stop"));
		scanner.close();
		_stop = true;
	}
	
	private static void loadFile() {
		File file = new File("tmp.dat");
		if (file.exists()) {
			if (!file.canWrite()) {
				throw new Assignment01Exception("Cannot write to file!");
			}
			file.delete();
		}
		try {
			_file = new RandomAccessFile(file, "rw");
			FileChannel channel = _file.getChannel();
			_mappedBuff = channel.map(MapMode.READ_WRITE, 0,50);
		} catch (Exception e) {
			throw new Assignment01Exception("Cannot write to file!");
		}
	}
	
	private static void operateOnFile() {
		try {
			if (_mappedBuff.get() == 0) {
				_mappedBuff.rewind();
				_mappedBuff.putChar(OperationMark.Continue.getChar());
				_mappedBuff.putChar(OperationMark.Write.getChar());
				_mappedBuff.putInt(_random.nextInt());
				_mappedBuff.putInt(_random.nextInt());
			}
			while (true) {
				_mappedBuff.rewind();
				if (_mappedBuff.getChar() == OperationMark.Continue.getChar()) {
					if (_mappedBuff.getChar() == OperationMark.Read.getChar()) {
						if (_stop) {
							_mappedBuff.rewind();
							_mappedBuff.putChar(OperationMark.Stop.getChar());
							break;
						} else {
							_mappedBuff.rewind();
							_mappedBuff.putChar(OperationMark.Continue.getChar());
							_mappedBuff.putChar(OperationMark.Write.getChar());
							_mappedBuff.putInt(_random.nextInt());
							_mappedBuff.putInt(_random.nextInt());
						}
					}
				} 
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Assignment01Exception("Cannot operate on file");
		}
	}
}
