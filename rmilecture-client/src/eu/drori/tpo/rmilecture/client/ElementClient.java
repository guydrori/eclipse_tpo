package eu.drori.tpo.rmilecture.client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import eu.drori.tpo.rmilecture.Configuration;
import eu.drori.tpo.rmilecture.Element;

public class ElementClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Registry registry = LocateRegistry.getRegistry(Configuration.PORT);
			Element element = (Element)registry.lookup(Configuration.BIND_NAME);
			System.out.println(element.getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
