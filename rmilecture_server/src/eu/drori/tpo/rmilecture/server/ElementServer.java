package eu.drori.tpo.rmilecture.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import eu.drori.tpo.rmilecture.Configuration;
import eu.drori.tpo.rmilecture.Element;

public final class ElementServer {

	public static void main(String[] args) {
		try {
			LocateRegistry.createRegistry(Configuration.PORT);
			Element element = new RemoteElement();
			Naming.bind(Configuration.BIND_NAME, element);
			System.out.println("Server started");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
