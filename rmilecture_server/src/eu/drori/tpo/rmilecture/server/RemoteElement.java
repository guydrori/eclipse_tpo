package eu.drori.tpo.rmilecture.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import eu.drori.tpo.rmilecture.Element;

public class RemoteElement extends UnicastRemoteObject implements Element {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4676089620952415011L;
	private String value;
	private static final String DEFAULT_VALUE = "HELLO";
	
	public RemoteElement() throws RemoteException {
		value = DEFAULT_VALUE;
	}
	
	public RemoteElement(String val) throws RemoteException {
		value = val;
	}
	
	@Override
	public void setValue(String str) throws RemoteException {
		// TODO Auto-generated method stub
		value = str;
	}

	@Override
	public String getValue() throws RemoteException {
		// TODO Auto-generated method stub
		return value;
	}

}
