package eu.drori.tpo.assignment3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/form/*")
public class FormServlet extends HttpServlet {

	/**
	 * 
	 */

	
	private static final String HTML_HEADER = "<html>\n<head>\n<title>Test servlet</title>\n</head>"
			+ "\n<body>";
	
	private static final String HTML_FOOTER = "</body>\n</html>";
	
	private static final long serialVersionUID = 1L;
	
	private static final String DIGIT_PATTERN_STRING = "\\d+";
	
	private static final Pattern DIGIT_PATTERN = Pattern.compile(DIGIT_PATTERN_STRING);
	
	public FormServlet() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		resp.setContentType("text/html; charset=UTF-8");
		String number1 = "0";
		String number2 = "0";
		
		if (!req.getParameterMap().keySet().isEmpty()) {
			number1 = req.getParameter("number1");
			number2 = req.getParameter("number2");
		}
		PrintWriter pw = resp.getWriter();
		pw.println(HTML_HEADER);
		
		pw.println("<h1>Assignment 3 Form</h1>");
		pw.println("<form>\nNumeric input 1: <br>");
		pw.println("<input type=\"text\" name=\"number1\" value=\"" + number1 + "\"><br><br>");
		pw.println("Numeric input 2: <br>");
		pw.println("<input type=\"text\" name=\"number2\" value=\"" + number2 + "\"><br><br>\n");
		pw.println("<button type=\"submit\" formmethod=\"get\">Submit GET</button>" +
		"\t<button type=\"submit\" formmethod=\"post\">Submit POST</button>\n</form>");
		
		if (!req.getParameterMap().keySet().isEmpty()) {
			if (DIGIT_PATTERN.matcher(number1).matches() && DIGIT_PATTERN.matcher(number2).matches()) {
				pw.println("<h2>Result</h2>");
				try {
					int int1 = Integer.parseInt(req.getParameter("number1"));
					int int2 = Integer.parseInt(req.getParameter("number2"));
					pw.println("Result = " + (int1 + int2));
				} catch (Exception e) {
					pw.println("Calculation failed :-(");
				}
			} else {
				pw.println("<p style=\"color: red\">At least one of the entered parameters is not an integer number!</p>");
			}
		}
		
		pw.println(HTML_FOOTER);
		pw.close();
	}
	
	
}
